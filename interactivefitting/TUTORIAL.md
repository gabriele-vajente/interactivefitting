## Step by step tutorial for InteractiveFitting()

### 0) Installation

See [INSTALL.md](../INSTALL.md) for installation info.

### 1) Load data
You have two options to pass transfer function data to the `InteractiveFitting()` function:

#### a. Pass python variables
You can use python commands to prepare

- `fr`: numpy.ndarray of float containing the frequency bins, size (n,)
- `tf`: numpy.ndarray of complex containing the value of the transfer function for each
        frequency bin (complex numbers), size (n,)
- `co`: numpy.ndarray of float, optional, containing the measurement coherence, size (n,)

With those variables, you can start the interactive fitting with the command

```
InteractiveFitting(fr, tf, coherence=co)
```

where passing the coherence is optional: if you don't, `InteractiveFitting` assumes the coherence
is one at all frequencies.

#### b. Load from files

If you call without any argument, a dialog will open that allows you to select text files
from which data can be loaded:

![Open dialog](pics/open_dialog.png)

You can either write a filename in the text entries, or click the "Select file..." button
to open a file selection dialog. 

Data is expected in text format, where every line corresponds to a frequency bins. Lines 
starting with # or with % are considered comments and ignored.
For the transfer function, you can select the text file format from the multiple options
shown below the file selection line. Selecting a coherence data file is optional.

You can set a minimum and maximum frequency: when data is loaded, only that range is used. 
This is a good thing to do when the transfer function has been measured up to very high frequency
but there is no good coherence above a certain value.

If you select the checkbox "Apply filter before fitting", then the filter defined in the 
textbox input is applied (in frequency domain) to the data. You can define any filter using
the `TF` class, that takes zeros, poles and gain in Laplace domain, and optionally a delay in
seconds.

Once files are selected, click "Load" to read the data and start the interactive fitting GUI.

Look in the folder `sample_data` for some example files:
- `dhard_y_tf.txt` and `dhard_y_co.txt` contains measured TF and coherence for the LHO DHARD Y plant
- `mich_ff.txt` contains a MICH LSC feedforward measurement from LHO
- `srcl_ff.txt` contains a SRCL LSC feedforward measurement from LHO

Finally, the button "Load saved session" allows you to load a fitting session that was previously 
saved. This will ignore all other datafiles and justt load the status of the fitting GUI that
was saved, including measurement data, fits, reference files, etc.

#### c. Passing an initial fit

The `InteractiveFitting()` class constructor can take an additional `fit=` argument. You can
use if to pass teh fit you want to work on. In this way you bypass running VectFit3, and you
can go directly to editing the fit.

The inital fit can be passed as
- a list `[z,p,k]`
- a dictionary `{'z':z, 'p':p, 'k':k}`
- a `TF` object

### 2) User Interface basics

![Initial GUI](pics/main_gui_initial.png)

This is the graphical user interface for the interactive fitting. The four main plot panels 
contain: on the left, the measurement data, absolute value on top and phase on bottom; on 
the right the top panel shows the measurement coherence, the bottom panel will show the fit
relative residual (fit - measurement)/measurement, once fits are available.

The coherence plot has a movable horizontal red line, that you can drag to set the threshold
value: only points where the coherence is above that value will be used in the fit. The points
above the coherence threshold are shown in dark blue in the transfer function plots.

At the top of the window there is a toolbar: at the moment only one button is visible, but
more will become available in later steps.

Finally, at the bottom of the window there is a status bar that will contain pertinent 
information depending on the actions being performed.

### 3) Select fitting ranges

To selet what data is used for the fitting, the first step is to set the coherence threshold
by dragging the red line in the coherence plot: the text near the line shows the current value.

Then you need to set the frequency range or ranges for the fit. Click on the "Add fit range" 
button. When you hover in the magnitude plot, a vertical line is visible. Click once to set 
the start frequency, move the mouse and click a second time to set the end frequency. The fit
area is shown as shaded in the plot. You can add multiple areas.

![Add fit range](pics/fit_ranges.png)

Once at least one area is added, you have the option to adjust it by grabbing the edges and
dragging them to a different position. You can also click on "Remove fit range" and then click
inside one of the fit ranges to delete it. The button "Show fit ranges" can be toggled to show
or hide the current fit areas.

### 4) Run VectFit3

With the coherence threshold set and the fit ranges added to the plot, you can now run
the VectFit3 algorithm to produce fits. When you click on "Run VectFit3", a dialog appears 
instructing you to provide the fit order or orders. The fit order is the number of poles
and of zeros used in the fit. VectFit3 always returns transfer functions with the same 
number of poles and zeros.

![VectFit dialog](pics/vectfit.png)

Valid entries are:

- single number: `8`
- list: `4,5,6,7` or `[4,5,6,7]`
- numpy (imported as `np`) and python commands: `np.arange(10)`

Most of the times this is all you need to set, since you want the transfer function to be expressed only in terms of
poles and zeros, as a rational transfer function. In some cases you might know that the transfer function you are fitting
includes a finite time delay. This cannot be expressed exactly in terms of poles and zeros. In such a case VectFit3 will
try to approximate the phase rotation with a combination of poles and zeros at high frequency. Instead, you can select
the option "Try to fit delay" and specify in the two text boxes the minimum and maximum delay allowed. With this option, the
fitting algorithm tries to find the delay that gives the smallest residual after VectFit3 finds the optimal fit. The delay
can be expressed in the results in two ways: you can leave it as a exact delay, and the resulting transfer function
cannot be expressed in terms of poles and zeros alone (so you cannot export it exactly to foton); you can instruct the
algorithm to substitute the delay with a Padé approximation made with poles and zeros.

Once you click "Ok", VectFit3 is run and the resulting fits are shown in the plots. Here 
for example we ran with orders `6,8,10,12`

![VectFit result](pics/vectfit_results.png)

The magnitude and phase plots show four new traces eac, corresponding to the fit for the
specified orders. The residual plot is now populated with the relative residual for each
fit. 

You can zoom and pan each of the plots using the mouse left and right buttons. The fit traces
are updated to always cover the entire range. Additionally, the fit magnitude and phase plots
are resampled, so if you zoom in nto features, you can resolve them at higher frequency 
resolution.

### 5) Select preferred fit

You can select the fit that works best for you by clicking on "Choose fit" and then clicking 
on the corresponding trace. If none of the fits are good, you can delete them all and try
again by clicking on "Clear fits".

Once one fit is selected, the plots change to display only that particular fit and the residual.

![Selected fit](pics/selected_fit.png)

In the magnitude plot, the position of poles and zeros is shown with markers. Circles are
zeros, stars are poles. Solid markers are complex poles or zeros, while outlined markers are
real poles or zeros. Zeros can have positive or negative frequency: zeros with positive
frequency are shown in red, while zeros with negative frequency are shown in blue.

### 6) Exporting the fit

By clicking on "Export fit", you can open a window that shows the foton command string
and the ZPK that describes the current fit (or any saved reference).

![Foton](pics/foton.png)


### 7) Interactively editing the fit

Now comes the best part. If you click on "Edit fit", a new toolbar appears with a set of
functions to interactively edit the fit.

![Edit toolbar](pics/edit_fit_toolbar.png)

If none of the buttons are selected, you can just zoom and pan in the plots as usual. 
Each function is activated by clicking on one of the buttons: as long as the button is
selected, the function is active.

If you click on one of the poles or zeros, the corresponding marker will become larger and
that pole or zero is selected. The status bar shows the frequency and Q.

![Selected pole](pics/selected.png)

#### a. Adding poles and zeros

The first four buttons are used to add poles and zeros to the fit transfer function. 
Just select one of the "Add complex pole", "Add real pole", "Add complex zero", 
"Add real zero", then click on the fit curve in the magnitude plot to add the pole or zero
at that frequency. Complex poles and zeros are added with an initial Q of `sqrt(2)/2`

![Added pole](pics/add_pole.png)

The magnitude, phase and residual plots are updated with the new pole or zero. If you don't 
like the result, you can click on "Undo" to undo the last action.

#### b. Deleting poles and zeros

With the "Delete zero/pole" button seleted, simply click on a zero or pole to delete it. 
Here for example we deleted the high frequency zeros that VectFit3 added to make the 
numerator and denominator of the transfer function of the same order.

![Deleted zeros](pics/deleted_zeros.png)

#### c. Moving poles and zeros

When you select the "Move pole/zero" button, you have two ways to adjust the frequency and
Q of a pole or zero.

- you can drag a marker to a different position, and the frequency and Q of the corresponding
  pole or zero is adjusted so that the peak follows the marker position. You might notice that
  in some cases you cannot drag the marker further down for a pole, or further up for a zero:
  that's because there is no solution with a positive Q to make the transfer function behave 
  the way you want. Also, if you drag a pole peak down (or a zero dip up) you might end up 
  with a complex pole or zero with a Q between 0 and 0.5: that is not complex anymore, but
  a pair of real poles or zeros. In that case, after you release the mouse button, the 
  marker you were moving disappears and is replaced by the two real poles or zeros that 
  correspond to the low Q

- if a pole or zero is selected, by clicking on a marker, then you can adjust its frequency
  and Q using the keyboard. Arrow Up and Down change the Q, Left and Right change the frequency.
  The status bar now has two input lines at the right, that allow you to adjust the increment.
  For Q you set the relative increment: setting 0.05 means that pressing the Arrow Up key
  will increase the Q by 5%. For frequency you set the relative increment in fractions of
  the currently displayed frequency range: 0.003 means that pressing Arrow Left will reduce
  move the pole or zero to the left (lower frequency) by 0.003 of the horizontal range of the
  plots.

![Move pole](pics/move_pole.png)

#### d. Changing the gain

When the "Change gain" button is selected, you can change the overall gain of the fit
transfer function. You have two ways to do this:

- By dragging the fit trace up or down
- By using the Arrow Up and Arrow Down keys. The gain increment can be set in the status
  bar, in db.

![Change gain](pics/change_gain.png)

#### e. Locking the gain at a certain frequency

You might have noticed that when you add a pole or zero, or move a pole or zero, by default
the low frequency gain of the transfer function is left unchanged as much as possible.

It is sometimes desirable to keep the transfer function gain fixed at a certain frequency.
This is what the "Lock gain at Frequency" button is for. When you select it, you can then
click anywhere on the fit trace. A black diamond marker appears there: the transfer function
gain at that frequency is now kept fixed at the current value, even when you move or add
poles and zeros. The gain changes only when you manually change the overall gain.

If you deselect the "Lock gain at Frequency" button, the current gain lock is removed and 
the transfer function gain is free to change.

![Lock gain](pics/lock_gain.png)

### 7) Edit poles and zeros directly

You can also select the "Edit ZPK" button. This will open a new window with the list
of the current pole and zero frequencies and Qs

![Edit ZPK](pics/editzpk.png)

You can change any value by double clicking on a number and changing it. Pressing return 
will confirm the change. If the change is not allowed (negative Q or negative frequency pole)
then your change is ignored. You can also delete the selected pole or zero by clicking on the
"Delete" button, or add a new pole or zero by clicking on the "Add" button: this will add a
new pole or zero at the end of the list, with initial frequency of 0 Hz and initial Q of 0.
In this list, a Q equal to 0 means a real pole or zero.

Clicking on "Done" closes the window and updates the plots in the main GUI. Clicking 
"Cancel" discards all the changes made.

### 8) Adding reference traces

It is possible to save the current fit as a reference, and display it in the figures. By 
clicking on "References" you open a menu with several options

![Reference menu](pics/reference_menu.png)

First you can click "Add reference": this will save the current fit, display it in the
magnitude, phase and residual plots, and add it to the menu with a check near it. 
References are numbers sequentially when added. If you uncheck any of the references in 
the menu, they will be hidden in the plots. For example in the figure below we added three
references, but only 1 and 3 are checked, so reference 2 is not shown.

![References](pics/references.png)

You can delete all the checked references by clicking on "Delete selected reference(s)":
all the reference traces that are checked are removed and lost forever.

Finally, you can revert the main editable fit poles and zeros to any of the saved references
by clicking n "Revert to first selected reference": if multiple references are selected,
the first one in the menu list is used.

### 9) Least square optimization of the current fit

By clicking on "Residual optimization" you can run a minimization routine that moves poles, 
zeros and gains around to minimize the residual inside the fitting ranges. 

Clicking on the button will open a dialog where you can set some of the optimization parameters.

![Optimization dialog](pics/optimization_dialog.png)

The "Optimization method" combo box allows you to select which optimization algorithm will be used
by `scipy.optimize.minimize`. The optimization starts with the current values of pole and zero 
frequencies and Q, and will by default allow a change of each by a factor of two: from 0.5 the 
current value to 2 the current value. You can change this range separately for frequency, Q and
overall gain in the "Relative change" text boxes. You can also specify absolute limits for the
value of the frequency and Q for both poles and zeros. When you specify both relative and absolute
bounds, the most stringent one for each frequency or Q is used.

The minimization uses a cost function the relative residual, as shown in the main plots,
integrated over the specified fit ranges. You can choose two ways to weight this residual over
frequencies: 
- "Linear-in-frequency" means that all frequencies bins weight equally
- "Logarithmic-in-frequency" means that frequencies are weighted with 1/f: this is equivalent
  to equally weighting all parts of the residual plot when shown with a log scale in the x axis. In
  other words the low frequencies are weighted more since they comprise a smaller fraction of the
  entire range.

If you check the "Advanced constraints" box you have access at a new set of parameters that allows you to force
the fit to have a magnitude smaller than a certain value in a specified frequency range. 

![Optimization dialog](pics/optimization_dialog_adv.png)

### 10) Saving the current progress

You can click on "Save session" to save the current fitting progress and reference traces to a file.
You can then load it back from the initial dialog you get when you call `InteractiveFitting()` without
arguments.
