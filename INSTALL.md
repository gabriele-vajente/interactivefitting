## Installation instructions

### Recommended from conda-forge
```
conda install -c conda-forge interactivefitting
```

### Manually install dependencies and development version

Install dependencies

```
conda create -n interactivefitting python=3.9
conda activate interactivefitting
conda install -c conda-forge numpy scipy pyqtgraph pyqt python-foton ipython
```

Clone the repository and from the main folder, run this to install
```
pip install .
```

Start InteractiveFitting
```
>> ipython
Python 3.9.18 | packaged by conda-forge | (main, Dec 23 2023, 16:35:41) 
Type 'copyright', 'credits' or 'license' for more information
IPython 8.18.1 -- An enhanced Interactive Python. Type '?' for help.

In [1]: from interactivefitting import *

In [2]: InteractiveFitting()
```
