# fit.py - InteractiveFitting, Gabriele Vajente (vajente@caltech.edu), 2024-01-28

import numpy as np
import pyqtgraph as pg
import scipy.optimize
from pyqtgraph.Qt import QtCore
from pyqtgraph.Qt import QtWidgets
from pyqtgraph.dockarea.Dock import Dock
from pyqtgraph.dockarea.DockArea import DockArea
from . import vectfit3
import warnings
warnings.filterwarnings('ignore')
from .tf import TF
from queue import LifoQueue
import sys
import pickle
#import foton

########################################################################################################################

class InteractiveFitting():
    """
    Graphical user interface to fit transfer functions with VectFit3. And to allow tuning the result by manually moving
    or adding/removing poles and zeros.

    Gabriele Vajente (vajente@caltech.edu) - 2024-01-06

    Parameters
    ----------
    fr: numpy.ndarray (float)
        frequency bins
    tf: numpy.ndarray (complex)
        transfer function values (complex number) for each frequency bin
    co: numpy.ndarray (float)
        optional, coherence value for each frequency bin
    fit: TransferFunction or [numpy.ndarray, numpy.ndarray, numpy.ndarray]
        optional, pass the fit, either as a TF object, or as a list [z, p, k]

    Examples
    --------

    `InteractiveFitting(fr, tf)` will open the GUI with the measurement defined by the frequency bins in `fr`
    and the complex transfer function `tf`. Once the GUI is opne, you can define the fitting region, run VectFit
    and tweak the results

    `InteractiveFitting(fr, tf, coherence=co)` same as above, but now you're passing the coherence vector `co` and you
    can adjust a coherence threshold for fitting in the GUI.

    `InteractiveFitting(fr, tf, fit=[z, p, k])` instead of running VectFit, you're passing a set of poles `p`, zeros `z`
    and gain `k` in s-domain, that defines the fit that you can tweak with the GUI.

    `InteractiveFitting()` when called without arguments, opens a dialog to select files from where data can be read.

    Please refer to the README.md file for instructions on how to use the GUI

    """
    def __init__(self, fr=None, tf=None, coherence=None, fit=None):
        """
        Class constructor

        Parameters
        ----------
        fr: numpy.ndarray (float)
            frequency bins
        tf: numpy.ndarray (complex)
            transfer function values (complex number) for each frequency bin
        co: numpy.ndarray (float)
            optional, coherence value for each frequency bin
        fit: TF or [numpy.ndarray, numpy.ndarray, numpy.ndarray]
            optional, pass the fit, either as a TransferFunction object, or as a list [z, p, k]
        """

        #### Initialize variables

        # save transfer function data and coherence
        session_filename = None
        if fr is None or tf is None:
            # if called without arguments, open a window to load from files
            app = QtWidgets.QApplication([])
            dialog = OpenFilesGUI()
            dialog.show()
            app.exec()
            # see if we need to load a session
            if dialog.session_filename is not None:
                # some fake values to move on, will load session later
                fr = np.logspace(0,2,100)
                tf = np.ones_like(fr, dtype=complex)
                session_filename = dialog.session_filename
            else:
                # use the data read from files if any
                if dialog.transferfunction is not None:
                    fr = dialog.fr
                    tf = dialog.transferfunction
                else:
                    return
                if dialog.coherence is not None:
                    coherence = dialog.coherence

        # else save values
        self.fr = fr
        self.tf = tf
        if coherence is not None:
            self.co = coherence
        else:
            self.co = np.ones_like(self.fr)

        # variables that will contain fits
        self.tf_fit = None
        self.fit_fr = None
        self.fits = None
        # internal flags
        self.fit_plotted = False
        self.fits_plotted = False
        # variables to contain graphical elements
        self.line_meas_abs1 = None
        self.line_meas_abs2 = None
        self.line_meas_coh = None
        self.line_meas_res = None
        self.line_fit_abs = None
        # legends
        self.legend_abs = None
        self.legend_phi = None
        self.legend_res = None

        # variables for reference fits
        self.reference_fits = []
        self.reference_items = []
        self.reference_plot_abs = []
        self.reference_plot_phi = []
        self.reference_plot_res = []
        
        self.fit_nfr = 1000        # number of points for the fit plots
        self.fit_update_thr = 0.1  # update fit plot when zoom changes this much

        # auxiliary variables to add and save the fit regions
        self.addFitAreaStep = 0    
        self.fitAreas = []

        # variable to allow keeping gain locked at a given frequency
        self.lock_gain_freq = None
        self.lock_gain_value = None
        self.lock_gain_dot = None

        # LIFO queue for undos
        self.undo = LifoQueue()

        # how much poles and zeros are moved with the keyboard
        self.frequency_increment = 0.003
        self.q_increment = 0.05
        self.gain_increment = 1

        #### create GUI

        # main app and window
        self.app = pg.mkQApp("Interactive Fitting")
        self.win = WindowKeyEvent(self)   # class defined below, inherited from QMainWindow, to handle keyboard events
        self.win.resize(1400,1000)
        self.win.setWindowTitle("Interactive Fitting")
        pg.setConfigOptions(antialias=True)
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')
        
        # toolbar at the top with buttons
        self.toolbar = QtWidgets.QToolBar("Toolbar")
        self.toolbar.toggleViewAction().setVisible(False)
        self.win.addToolBar(self.toolbar)
        self.bAddArea = QtWidgets.QAction("Add fit range", self.win)
        self.bAddArea.setToolTip('Add a fit frequency range by clicking first '
                                 'on the start frequency and then on the end frequency. '
                                 'You can add multiple ranges')
        self.bAddArea.setCheckable(True)
        self.toolbar.addAction(self.bAddArea)
        self.bRemoveArea = QtWidgets.QAction("Remove fit range", self.win)
        self.bRemoveArea.setCheckable(True)
        self.bRemoveArea.setToolTip("Remove a fit frequency range. Just click inside it")
        self.toolbar.addAction(self.bRemoveArea)
        self.bRemoveArea.setVisible(False)
        self.bShowArea = QtWidgets.QAction("Show fit ranges", self.win)
        self.bShowArea.setCheckable(True)
        self.bShowArea.setToolTip("Toggle display of the fit ranges")
        self.toolbar.addAction(self.bShowArea)
        self.bShowArea.setVisible(False)
        self.bShowArea.setChecked(True)
        self.bVectFit = QtWidgets.QAction("Run VectFit3", self.win)
        self.toolbar.addAction(self.bVectFit)
        self.bVectFit.setToolTip("Now that fit areas are defined, you can run VectFit. A dialog will "
                                 "open to let you specify the fit order(s). \nDon't forget to set the coherence "
                                 "threshold in the coherence plot by dragging the red horizontal line")
        self.bVectFit.setVisible(False)
        self.bClearFit = QtWidgets.QAction("Clear Fits", self.win)
        self.bClearFit.setToolTip("Remove all currently displayed fits, allowing you to run VectFit again")
        self.toolbar.addAction(self.bClearFit)
        self.bClearFit.setVisible(False)
        self.bChooseFit = QtWidgets.QAction("Choose fit", self.win)
        self.bChooseFit.setToolTip("Select the fit you like, by clicking on its trace in the magnidute plot. "
                                   "Once selected you'll be able to edit and export the fit")
        self.toolbar.addAction(self.bChooseFit)
        self.bChooseFit.setVisible(False)
        self.bChooseFit.setCheckable(True)
        self.bExportFit = QtWidgets.QAction("Export fit", self.win)
        self.toolbar.addAction(self.bExportFit)
        self.bExportFit.setToolTip("Open a window showing the foton design string and ZPK for the current fit")
        self.bExportFit.setVisible(False)
        # self.bPrintFit = QtWidgets.QAction("Print zpk", self.win)
        # self.bPrintFit.setToolTip("Print to terminal a z,p,k representation of the current fit, as well "
        #                           "as the list of poles and zeros. Q=0 means real pole or zero")
        # self.toolbar.addAction(self.bPrintFit)
        # self.bPrintFit.setVisible(False)
        self.bEditFit = QtWidgets.QAction("Edit fit", self.win)
        self.bEditFit.setToolTip("Add a whole new toolbar that allows you to edit the current fit")
        self.toolbar.addAction(self.bEditFit)
        self.bEditFit.setCheckable(True)  
        self.bEditFit.setVisible(False)
        self.exclusive_group = QtWidgets.QActionGroup(self.win)
        self.exclusive_group.addAction(self.bAddArea)
        self.exclusive_group.addAction(self.bRemoveArea)
        self.exclusive_group.addAction(self.bChooseFit)
        self.exclusive_group.addAction(self.bEditFit)     
        self.exclusive_group.setExclusionPolicy(QtWidgets.QActionGroup.ExclusionPolicy.ExclusiveOptional)

        # tutorial button
        self.bTutorial = QtWidgets.QAction("Tutorial", self.win)
        self.bTutorial.setToolTip("Open the TUTORIAL.md file in a new window")
        self.toolbar.addAction(self.bTutorial)
        self.bTutorial.setVisible(True)
        self.bTutorial.triggered.connect(self.open_tutorial)

        # connect event handlers to buttons
        self.bVectFit.triggered.connect(self.run_vectfit)
        self.bClearFit.triggered.connect(self.clear_fits)
        self.bExportFit.triggered.connect(self.export_fit)
        #self.bPrintFit.triggered.connect(self.print_fit)
        self.bEditFit.toggled.connect(self.edit_fit)
        self.bShowArea.toggled.connect(self.show_areas)
        self.bAddArea.toggled.connect(self.add_area_toggle)
        self.bChooseFit.toggled.connect(self.select_fit_toggle)

        # add the fit edit toolbar
        self.win.addToolBarBreak()
        self.edit_toolbar = QtWidgets.QToolBar("EditToolbar")
        self.toolbar.toggleViewAction().setVisible(False)
        self.win.addToolBar(self.edit_toolbar)
        self.edit_toolbar.hide()
        self.bFitAddCmpxPole = QtWidgets.QAction("Add complex pole", self.win)
        self.bFitAddCmpxPole.setToolTip("Add a complex pole to the current fit. Click on the fit curve to select the "
                                        "frequency. \nThe pole is added with an initial Q of 1/sqrt(2). As long as the"
                                        " button is selected, you can add more poles")
        self.bFitAddCmpxPole.setCheckable(True)
        self.edit_toolbar.addAction(self.bFitAddCmpxPole)
        self.bFitAddRealPole = QtWidgets.QAction("Add real pole", self.win)
        self.bFitAddRealPole.setCheckable(True)
        self.bFitAddRealPole.setToolTip("Add a real pole to the current fit. Click on the fit curve to select the "
                                        "frequency. \nAs long as the button is selected, you can add more poles")
        self.edit_toolbar.addAction(self.bFitAddRealPole) 
        self.bFitAddCmpxZero = QtWidgets.QAction("Add complex zero", self.win)
        self.bFitAddCmpxZero.setToolTip("Add a complex zero to the current fit. Click on the fit curve to select the "
                                        "frequency. \nThe zero is added with an initial Q of 1/sqrt(2). As long as the"
                                        " button is selected, you can add more zeros")
        self.bFitAddCmpxZero.setCheckable(True)
        self.edit_toolbar.addAction(self.bFitAddCmpxZero)
        self.bFitAddRealZero = QtWidgets.QAction("Add real zero", self.win)
        self.bFitAddRealZero.setCheckable(True)
        self.bFitAddRealZero.setToolTip("Add a real zero to the current fit. Click on the fit curve to select the "
                                        "frequency. \nAs long as the button is selected, you can add more zeros")
        self.edit_toolbar.addAction(self.bFitAddRealZero)    
        self.bFitDelete = QtWidgets.QAction("Delete zero/pole", self.win)
        self.bFitDelete.setToolTip("Delete a pole or zero from the current fit, just click on it. ")
        self.bFitDelete.setCheckable(True)
        self.edit_toolbar.addAction(self.bFitDelete)          
        self.bFitMovePZ = QtWidgets.QAction("Move pole/zero", self.win)
        self.bFitMovePZ.setToolTip("Move a pole or zero. You can either drag the marker around with your mouse, or "
                                   "select a pole or zero by clicking on a marker, \nand then use the keyboard to "
                                   "change frequency (left and right keys) or Q (up and down keys). The frequency and "
                                   "Q increment from keyboard can be adjusted in the status bar.")
        self.bFitMovePZ.setCheckable(True)
        self.edit_toolbar.addAction(self.bFitMovePZ)            
        self.bFitChangeGain = QtWidgets.QAction("Change Gain", self.win)
        self.bFitChangeGain.setCheckable(True)
        self.bFitChangeGain.setToolTip("Change the overall gain, either by dragging the fit line up or down, or using "
                                       "the keyboard: up and down keys for small adjustments, \nand page up and page "
                                       "down for larger adjustments. The gain change from keyboad can be adjusted in "
                                       "the status bar.")
        self.edit_toolbar.addAction(self.bFitChangeGain)
        self.bFitLockGain = QtWidgets.QAction("Lock Gain at Frequency", self.win)
        self.bFitLockGain.setToolTip('Lock the fit gain at a specified frequency. Initially, when this button is '
                                     'selected, you can click anywhere on the fit curve \nand the gain at that '
                                     ' frequency will remain locked at the current value when you add or remove '
                                     ' poles and zeros. \nA black diamond marker shows the lock point. To disengage '
                                     ' simply deselect this button.')
        self.bFitLockGain.setCheckable(True)
        self.edit_toolbar.addAction(self.bFitLockGain)
        self.bFitUndo = QtWidgets.QAction("Undo", self.win)
        self.bFitUndo.setToolTip("Undo the last action")
        self.bFitUndo.setCheckable(False)
        self.edit_toolbar.addAction(self.bFitUndo)
        self.bFitEditZPK = QtWidgets.QAction("Edit ZPK", self.win)
        self.bFitEditZPK.setToolTip("Open a dialog that allows you to change the value of all poles and zeros and gain")
        self.bFitEditZPK.setCheckable(False)
        self.edit_toolbar.addAction(self.bFitEditZPK)
        self.bFitLSQ = QtWidgets.QAction("Residual Optimization", self.win)
        self.bFitLSQ.setToolTip("Run a minimization on the residual integrated over the fit range"
                                " by allowing all poles and zeros to move")
        self.bFitLSQ.setCheckable(False)
        self.edit_toolbar.addAction(self.bFitLSQ)

        self.edit_exclusive_group = QtWidgets.QActionGroup(self.win)
        self.edit_exclusive_group.setExclusionPolicy(QtWidgets.QActionGroup.ExclusionPolicy.ExclusiveOptional)
        self.edit_exclusive_group.addAction(self.bFitAddCmpxPole)
        self.edit_exclusive_group.addAction(self.bFitAddRealPole)
        self.edit_exclusive_group.addAction(self.bFitAddCmpxZero)
        self.edit_exclusive_group.addAction(self.bFitAddRealZero)
        self.edit_exclusive_group.addAction(self.bFitDelete)
        self.edit_exclusive_group.addAction(self.bFitMovePZ)
        self.edit_exclusive_group.addAction(self.bFitChangeGain)

        # menu for fit references
        self.ref_button = QtWidgets.QToolButton()
        self.ref_button.setText("References")
        self.ref_button.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.ref_button.setToolTip("Open a menu to manage reference traces. If you select 'Add reference' a new "
                                   "trace is added that will keep the shape of the current fit. You can add multiple "
                                   "traces: \nthey will be listed at the bottom of this menu; you can deselect any of "
                                   "them to hide the corresponding trace. If you select 'Delete selected reference(s)'"
                                   " then all selected reference traces will be deleted. \nIf you select 'Revert to "
                                   "first selected reference' then the current fit is replaced by the first selected"
                                   " reference")
        self.edit_toolbar.addWidget(self.ref_button)
        self.ref_menu = QtWidgets.QMenu()
        self.ref_menu_add = QtWidgets.QAction('Add reference')
        self.ref_menu.addAction(self.ref_menu_add)
        self.ref_menu_delete = QtWidgets.QAction('Delete selected reference(s)')
        self.ref_menu.addAction(self.ref_menu_delete)
        self.ref_menu_revert = QtWidgets.QAction('Revert to first selected reference')
        self.ref_menu.addAction(self.ref_menu_revert)
        self.ref_menu.addSeparator()
        self.reference_items = []
        self.ref_button.setMenu(self.ref_menu)
        self.ref_menu_add.triggered.connect(self.add_reference)
        self.ref_menu_delete.triggered.connect(self.delete_references)
        self.ref_menu_revert.triggered.connect(self.revert_reference)

        self.bFitSave = QtWidgets.QAction("Save session", self.win)
        self.bFitSave.setToolTip("Save the current fitting session to a file")
        self.bFitSave.setCheckable(False)
        self.edit_toolbar.addAction(self.bFitSave)

        # add event handlers for toggle and click
        self.bFitAddCmpxPole.toggled.connect(self.curve_edit_toggle)
        self.bFitAddRealPole.toggled.connect(self.curve_edit_toggle)
        self.bFitAddCmpxZero.toggled.connect(self.curve_edit_toggle)
        self.bFitAddRealZero.toggled.connect(self.curve_edit_toggle)
        self.bFitChangeGain.toggled.connect(self.curve_edit_toggle)
        self.bFitLockGain.toggled.connect(self.curve_lockgain_toggle)
        self.bFitDelete.toggled.connect(self.curve_edit_toggle)
        self.bFitUndo.triggered.connect(self.undo_change)
        self.bFitEditZPK.triggered.connect(self.edit_zpk)
        self.bFitMovePZ.toggled.connect(self.toggle_move_zp)
        self.bFitChangeGain.toggled.connect(self.toggle_gain)
        self.bFitLSQ.triggered.connect(self.leastsquare_optimization)
        self.bFitSave.triggered.connect(self.save)

        # Add status bar at the bottom of the window
        self.status_bar = QtWidgets.QStatusBar()
        self.win.setStatusBar(self.status_bar)
        # add some widgets to the status bar
        self.status_bar_flabel = QtWidgets.QLabel('Frequency increment (fraction of range)')
        self.status_bar_finput = QtWidgets.QLineEdit()
        self.status_bar_finput.setText('%.3f' % self.frequency_increment)
        self.status_bar_finput.setFixedWidth(70)
        self.status_bar_qlabel = QtWidgets.QLabel('Q increment (relative)')
        self.status_bar_qinput = QtWidgets.QLineEdit()
        self.status_bar_qinput.setText('%.3f' % self.q_increment)
        self.status_bar_qinput.setFixedWidth(70)
        self.status_bar_gainlabel = QtWidgets.QLabel('Gain increment (db)')
        self.status_bar_gaininput = QtWidgets.QLineEdit()
        self.status_bar_gaininput.setText('%.3f' % self.gain_increment)
        self.status_bar_gaininput.setFixedWidth(70)
        self.status_bar.insertPermanentWidget(0, self.status_bar_finput)
        self.status_bar.insertPermanentWidget(0, self.status_bar_flabel)
        self.status_bar.insertPermanentWidget(0, self.status_bar_qinput)
        self.status_bar.insertPermanentWidget(0, self.status_bar_qlabel)
        self.status_bar.insertPermanentWidget(0, self.status_bar_gaininput)
        self.status_bar.insertPermanentWidget(0, self.status_bar_gainlabel)
        self.status_bar_finput.hide()
        self.status_bar_flabel.hide()
        self.status_bar_qinput.hide()
        self.status_bar_qlabel.hide()
        self.status_bar_gaininput.hide()
        self.status_bar_gainlabel.hide()
        self.status_bar_finput.editingFinished.connect(self.finput_textChanged)
        self.status_bar_qinput.editingFinished.connect(self.qinput_textChanged)
        self.status_bar_gaininput.editingFinished.connect(self.gaininput_textChanged)

        # main plot area using pyqtgraph DockArea to allow resizing
        self.area = DockArea()
        self.win.setCentralWidget(self.area)
        dock_abs = Dock('Magnitude')
        dock_phi = Dock("Phase")
        dock_coh = Dock("Coherence")
        dock_res = Dock("Residual")
        dock_abs.hideTitleBar()
        dock_phi.hideTitleBar()
        dock_coh.hideTitleBar()
        dock_res.hideTitleBar()
        self.area.addDock(dock_abs, 'left')
        self.area.addDock(dock_coh, 'right')
        self.area.addDock(dock_phi, 'bottom', dock_abs)
        self.area.addDock(dock_res, 'bottom', dock_coh)

        self.plot_abs = pg.PlotWidget()
        self.plot_phi = pg.PlotWidget()
        self.plot_coh = pg.PlotWidget()
        self.plot_res = pg.PlotWidget()
        self.plot_abs.disableAutoRange()
        self.plot_phi.disableAutoRange()
        self.plot_coh.disableAutoRange()
        self.plot_res.disableAutoRange()
        
        self.plot_abs.showGrid(x=True, y=True, alpha=1)
        self.plot_phi.showGrid(x=True, y=True, alpha=1)
        self.plot_coh.showGrid(x=True, y=True, alpha=1)
        self.plot_res.showGrid(x=True, y=True, alpha=1)
        
        self.plot_phi.setXLink(self.plot_abs)
        self.plot_coh.setXLink(self.plot_abs)
        self.plot_res.setXLink(self.plot_coh)
        
        self.plot_abs.setTitle('Transfer function [magnitude]')
        self.plot_phi.setTitle('Transfer function [phase]')
        self.plot_coh.setTitle('Coherence')
        self.plot_res.setTitle('Residual')
        self.plot_abs.setLabel('left', "Magnitude", units='abs')
        self.plot_abs.setLabel('bottom', "Frequency", units='Hz')
        self.plot_phi.setLabel('left', "Phase", units='deg.')
        self.plot_phi.setLabel('bottom', "Frequency", units='Hz')       
        self.plot_coh.setLabel('left', "Coherence")
        self.plot_coh.setLabel('bottom', "Frequency", units='Hz')
        self.plot_res.setLabel('left', "Residual")
        self.plot_res.setLabel('bottom', "Frequency", units='Hz')
        
        self.plot_abs.setLogMode(x=True, y=True)
        self.plot_phi.setLogMode(x=True, y=False)
        self.plot_coh.setLogMode(x=True, y=False)
        self.plot_res.setLogMode(x=True, y=True)

        # disable use of SI prefix (k, m, M, etc)
        self.plot_abs.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_phi.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_coh.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_res.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.plot_abs.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        self.plot_res.getPlotItem().getAxis('left').enableAutoSIPrefix(False)

        dock_abs.addWidget(self.plot_abs)
        dock_phi.addWidget(self.plot_phi)
        dock_coh.addWidget(self.plot_coh)
        dock_res.addWidget(self.plot_res)

        # connect zoom signal to resample fir frequency bins
        self.plot_abs.sigXRangeChanged.connect(self.zoom)

        # connect mouse click event to allow setting fit areas
        self.proxy1 = pg.SignalProxy(self.plot_abs.scene().sigMouseClicked, rateLimit=60, slot=self.mouseClicked)
        
        # infrastructure to handle vertical line (used during fit area selection)
        self.vLine = pg.InfiniteLine(angle=90, movable=False, pen='r')
        self.hLine = pg.InfiniteLine(angle=0, movable=False, pen='r')
        self.vLine.setVisible(False)
        self.hLine.setVisible(False)
        self.plot_abs.addItem(self.vLine, ignoreBounds=True)
        self.plot_abs.addItem(self.hLine, ignoreBounds=True)
        self.proxy2 = pg.SignalProxy(self.plot_abs.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)
        
        # movable line for coherence threshold
        self.coh_hLine = pg.InfiniteLine(angle=0, movable=True, pen='r', hoverPen=pg.mkPen('r', width=2),
                                         label='threshold={value:0.4f}',
                                         labelOpts={'position':0.1, 'color': 'r',  'movable': True}, bounds=[0, 1])
        self.plot_coh.addItem(self.coh_hLine)
        self.coh_hLine.setPos(0.9)
        self.coh_hLine.sigPositionChangeFinished.connect(self.plotMeasurements)

        if fit is not None:
            # populate with fit and activate all the right buttons
            self.set_fit(fit)

        #### Start app and GUI

        # will be used later for the optimization GUI
        self.opt_gui = None

        if session_filename is not None:
            # load a previously saved session, if specified
            self.load(session_filename)

        # plot measurement
        self.plotMeasurements()
 
        # start Qt app
        self.win.showMaximized()
        pg.exec()

    def set_fit(self, fit):
        """
        Set the initial fit (as passed by the user) and activate the right buttons

        Parameters
        ----------
        fit: TransferFunction or [numpy.ndarray, numpy.ndarray, numpy.ndarray]
            initial fit, either a TransferFunction object or a dictionary with keys 'z', 'p', 'k', or a list
            [z,p,k] pointing to zeros z, poles p and gain k.
        """

        # check what format is passed
        if not isinstance(fit, TF):
            if isinstance(fit, dict):
                fit = TF(fit['z'], fit['p'], fit['k'])   # dictionary
            elif isinstance(fit, list) or isinstance(fit, tuple) or isinstance(fit, np.ndarray):
                fit = TF(fit[0], fit[1], fit[2])         # list
            else:
                fit = TF()      # TransferFunction object
        # save fit
        self.tf_fit = fit
        # disable autorange of all plots
        self.plot_res.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_abs.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_phi.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_coh.disableAutoRange(pg.ViewBox.XAxis)
        # resample fiit frequency bins
        self.fit_fr = np.logspace(np.log10(self.fr[1]), np.log10(self.fr[-1]), self.fit_nfr)
        self.plot_abs.setXRange(np.log10(self.fit_fr[0]), np.log10(self.fit_fr[-1]))
        # build the object that handles movable markers for poles and zeros
        self.tf_graph = TranferFunctionGraph(self.tf_fit, self)
        self.plot_abs.addItem(self.tf_graph)

        # replot measurements (and fit)
        self.plotMeasurements()
        # disable and enable the proper buttons and toolbars
        self.bChooseFit.setChecked(False)
        self.fit_plotted = True
        self.bClearFit.setVisible(False)
        self.bChooseFit.setVisible(False)
        self.bShowArea.setChecked(False)
        self.bShowArea.setVisible(True)
        self.bAddArea.setVisible(True)
        self.bRemoveArea.setVisible(True)
        self.bEditFit.setVisible(True)
        self.bVectFit.setVisible(False)
        self.bExportFit.setVisible(True)
        #self.bPrintFit.setVisible(True)
        self.app.restoreOverrideCursor()

    def save(self):
        """
        Save the current status to a file, after opening a dialog to select the filename
        """

        # make a dictionary of stuff to save
        tosave = {
            'fr': self.fr,
            'tf': self.tf,
            'co': self.co,
            'tf_fit':self.tf_fit,
            'fit_fr': self.fit_fr,
            'cohe_thr': self.coh_hLine.getPos()[1],
            'reference_fits': self.reference_fits,
            'reference_names': [r.text() for r in self.reference_items],
            'reference_visible': [r.isChecked() for r in self.reference_items],
            'fitAreas': [r.getRegion() for r in self.fitAreas],
            'lock_gain_freq': self.lock_gain_freq,
            'lock_gain_value': self.lock_gain_value,
            'frequency_increment': self.frequency_increment,
            'q_increment': self.q_increment,
            'gain_increment': self.gain_increment,
            'layout': self.area.saveState()
        }

        # plot ranges
        tosave['ranges'] = {'abs': self.plot_abs.getViewBox().viewRange(),
                            'phi': self.plot_phi.getViewBox().viewRange(),
                            'coh': self.plot_coh.getViewBox().viewRange(),
                            'res': self.plot_res.getViewBox().viewRange()}

        # ask filename
        filename, ok = QtWidgets.QFileDialog.getSaveFileName(self.win, "Save session to file", "", "All Files (*)")
        # save to file
        if ok:
            pickle.dump(tosave, open(filename, 'wb'))

    def load(self, filename):
        """
        Load status from file and update

        Parameters
        filename: str
            name of the file where the current status will be saved
        """
        loaded = pickle.load(open(filename, 'rb'))
        self.fr = loaded['fr']
        self.tf = loaded['tf']
        self.co = loaded['co']
        self.tf_fit = loaded['tf_fit']
        self.fit_fr = loaded['fit_fr']
        self.reference_fits = loaded['reference_fits']
        reference_names = loaded['reference_names']
        reference_visible = loaded['reference_visible']
        fitAreas = loaded['fitAreas']
        self.lock_gain_freq = loaded['lock_gain_freq']
        self.lock_gain_value = loaded['lock_gain_value']
        self.frequency_increment = loaded['frequency_increment']
        self.q_increment = loaded['q_increment']
        self.gain_increment = loaded['gain_increment']

        # coherence threashold
        self.coh_hLine.setPos(loaded['cohe_thr'])

        # convert ranges to fit areas
        for f in fitAreas:
            lr = pg.LinearRegionItem([f[0], f[1]], brush=(255,240,240))
            lr.setZValue(-10)
            self.fitAreas.append(lr)
            self.plot_abs.addItem(lr)

        # add reference menu items
        for n, r, v in zip(reference_names, self.reference_fits, reference_visible):
            self.reference_items.append(QtWidgets.QAction(n, checkable=True))
            self.reference_items[-1].setChecked(v)
            self.reference_items[-1].toggled.connect(self.plotMeasurements)
            self.ref_menu.addAction(self.reference_items[-1])

        # update all plots
        self.plot_abs.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_phi.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_coh.disableAutoRange(pg.ViewBox.XAxis)
        self.plot_res.disableAutoRange(pg.ViewBox.XAxis)
        self.tf_graph = TranferFunctionGraph(self.tf_fit, self)
        self.plot_abs.addItem(self.tf_graph)
        self.plotMeasurements()

        # set ranges
        self.plot_abs.setXRange(loaded['ranges']['abs'][0][0],
                                loaded['ranges']['abs'][0][1])
        self.plot_abs.setYRange(loaded['ranges']['abs'][1][0],
                                loaded['ranges']['abs'][1][1])
        self.plot_phi.setYRange(loaded['ranges']['phi'][1][0],
                                loaded['ranges']['phi'][1][1])
        self.plot_res.setYRange(loaded['ranges']['res'][1][0],
                                 loaded['ranges']['res'][1][1])
        self.plot_coh.setYRange(loaded['ranges']['coh'][1][0],
                                 loaded['ranges']['coh'][1][1])

        self.fit_plotted = True

        # fit selected, activate and deactivate the proper buttons
        self.bShowArea.setVisible(True)
        self.bShowArea.setChecked(False)
        self.bClearFit.setVisible(False)
        self.bChooseFit.setVisible(False)
        self.bShowArea.setChecked(False)
        self.bShowArea.setVisible(True)
        self.bAddArea.setVisible(True)
        self.bRemoveArea.setVisible(True)
        self.bEditFit.setVisible(True)
        self.bVectFit.setVisible(False)
        self.bExportFit.setVisible(True)
        #self.bPrintFit.setVisible(True)
        self.app.restoreOverrideCursor()
        self.edit_toolbar.show()

    #### Functions to handle events

    def open_tutorial(self):
        # Create a new window with grid layout
        q = QtWidgets.QWidget()
        q.resize(1000, 800)
        g = QtWidgets.QGridLayout()
        q.setLayout(g)
        # central widget text box
        tutorial = QtWidgets.QTextEdit()
        g.addWidget(tutorial, 0, 0)
        # open file
        import pathlib
        filename = str(pathlib.Path(__file__).parent.resolve()) + '/TUTORIAL.md'
        md = open(filename, 'r').read()
        # substitute pointers to images
        md = md.replace('pics/', str(pathlib.Path(filename).parent.resolve()) +'/pics/')
        tutorial.setMarkdown(md)

        # open the window and wait for it to close
        q.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        q.show()
        loop = QtCore.QEventLoop()
        q.destroyed.connect(loop.quit)
        loop.exec()

    def undo_change(self):
        """
        Called when te Undo button is clicked
        """
        if not self.undo.empty():
            # there's something in the Undo list, so put it back and update all plots
            self.tf_fit = self.undo.get()
            self.tf_graph.updateTF(self.tf_fit)
            self.plotMeasurements()

    def debug(self):
        """
        Debugging function, print out transfer function
        """
        print('k', self.tf_fit.k)
        print('p/2/pi', self.tf_fit.p/2/np.pi)
        print('z/2/pi', self.tf_fit.z/2/np.pi)
        print('num', self.tf_fit.num)
        print('den', self.tf_fit.den)
    
    def add_area_toggle(self):
        """
        Change cursor when adding fit area
        """
        if self.bAddArea.isChecked():
            self.app.setOverrideCursor(QtCore.Qt.SplitHCursor)
        else:
            self.app.restoreOverrideCursor()
            
    def select_fit_toggle(self):
        """
        Change cursor when selecting fit
        """
        if self.bChooseFit.isChecked():
            self.app.setOverrideCursor(QtCore.Qt.PointingHandCursor)
        else:
            self.app.restoreOverrideCursor()
    
    def curve_edit_toggle(self):
        """
        Change cursor and make fit line clickable when edit buttons are active
        """
        self.app.restoreOverrideCursor()
        if self.bFitAddCmpxPole.isChecked() or self.bFitAddRealPole.isChecked() or \
                self.bFitAddCmpxZero.isChecked() or self.bFitAddRealZero.isChecked() \
                or self.bFitChangeGain.isChecked():
            self.app.setOverrideCursor(QtCore.Qt.CrossCursor)
            self.line_fit_abs.curve.setClickable(True)
        else:
            self.app.restoreOverrideCursor()
            self.line_fit_abs.curve.setClickable(False)
        if self.bFitDelete.isChecked():
            self.app.setOverrideCursor(QtCore.Qt.CrossCursor)
        else:
            self.app.restoreOverrideCursor()

    def add_reference(self):
        """
        Add a reference fit trace, called by the Reference menu
        """
        # add fit to the references list

        ref, ok = QtWidgets.QInputDialog.getText(self.win, "Reference name", 'Reference Name',
                                       text='Reference %d' % (len(self.reference_fits)+1))
        if ok:
            self.reference_fits.append(self.tf_fit.copy())
            # create new menu item
            self.reference_items.append(QtWidgets.QAction(ref, checkable=True))
            self.reference_items[-1].setChecked(True)
            self.reference_items[-1].toggled.connect(self.plotMeasurements)
            self.ref_menu.addAction(self.reference_items[-1])
            # replot
            self.plotMeasurements()

    def delete_references(self):
        """
        Delete all checked references, called when the user click on the Delete references menu item.
        """
        # find the items that are selected
        remove = [(i, ri) for i, ri in enumerate(zip(self.reference_items,
                                                     self.reference_fits, self.reference_plot_abs,
                                                     self.reference_plot_phi,
                                                     self.reference_plot_res)) if ri[0].isChecked()]
        # remove them from menu and lists adn plots
        for i,r in remove:
            ri, rf, rpa, rpp, rpr = r
            self.ref_menu.removeAction(ri)
            self.reference_items.remove(ri)
            self.reference_fits.remove(rf)
            self.plot_abs.removeItem(rpa)
            self.plot_phi.removeItem(rpp)
            self.plot_res.removeItem(rpr)
        # replot
        self.plotMeasurements()

    def revert_reference(self):
        """
        Revert transfer function to first selected reference
        """
        # find the first selected item
        for i,r in enumerate(self.reference_items):
            if r.isChecked():
                break
        # save transfer function to undo stack
        self.undo.put(self.tf_fit.copy())
        # revert to reference transfer function
        self.tf_fit = self.reference_fits[i].copy()
        # update graphs
        self.plotMeasurements()
        self.tf_graph.updateTF(self.tf_fit)

    def curve_lockgain_toggle(self):
        """
        Called when the Lock gain buttton is toggled
        """
        self.app.restoreOverrideCursor()
        if self.bFitLockGain.isChecked() and self.lock_gain_freq is None:
            # clicked on Lock gain to set the frequency, disable other actions
            self.bFitAddCmpxPole.setChecked(False)
            self.bFitAddRealPole.setChecked(False)
            self.bFitAddCmpxZero.setChecked(False)
            self.bFitAddRealZero.setChecked(False)
            self.bFitChangeGain.setChecked(False)
            self.bFitDelete.setChecked(False)
            self.line_fit_abs.curve.setClickable(True)
        if not self.bFitLockGain.isChecked():
            # disabled the Lock Gain button, so clear the gain lock variables
            self.lock_gain_freq = None
            self.lock_gain_value = None
            if self.lock_gain_dot is not None:
                self.plot_abs.removeItem(self.lock_gain_dot)
            self.lock_gain_dot = None
            self.plotMeasurements()
            self.line_fit_abs.curve.setClickable(False)

    def toggle_move_zp(self):
        """
        Called when the Move button is toggled, shows or hide the status bar widgets
        """
        if self.bFitMovePZ.isChecked():
            # show status bas widgets if something is selected
            if self.tf_graph.selected:
                self.status_bar_finput.show()
                self.status_bar_finput.setText('%.4f' % self.frequency_increment)
                self.status_bar_flabel.show()
                self.status_bar_qinput.show()
                self.status_bar_qinput.setText('%.4f' % self.q_increment)
                self.status_bar_qlabel.show()
            else:
                self.status_bar_finput.hide()
                self.status_bar_flabel.hide()
                self.status_bar_qinput.hide()
                self.status_bar_qlabel.hide()
        else:
            self.status_bar_finput.hide()
            self.status_bar_flabel.hide()
            self.status_bar_qinput.hide()
            self.status_bar_qlabel.hide()

    def toggle_gain(self):
        """
        Called when the Change gain button is toggled, shows or hide the status bar widgets
        """
        if self.bFitChangeGain.isChecked():
            self.status_bar_gaininput.show()
            self.status_bar_gaininput.setText('%.4f' % self.gain_increment)
            self.status_bar_gainlabel.show()
        else:
            self.status_bar_gaininput.hide()
            self.status_bar_gainlabel.hide()

    def finput_textChanged(self):
        """
        Called when the frequency increment input is changed
        """
        try:
            new_gain = float(self.status_bar_finput.text())
            if new_gain > 0 and new_gain < 1:
                self.frequency_increment = new_gain
        except:
            self.status_bar_finput.setText('%.4f' % self.frequency_increment)

    def qinput_textChanged(self):
        """
        Called when the frequency increment input is changed
        """
        try:
            new_gain = float(self.status_bar_qinput.text())
            if new_gain > 0 and new_gain < 1:
                self.q_increment = new_gain
        except:
            self.status_bar_qinput.setText('%.4f' % self.q_increment)

    def gaininput_textChanged(self):
        """
        Called when the frequency increment input is changed
        """
        try:
            new_gain = float(self.status_bar_gaininput.text())
            if new_gain > 0:
                self.gain_increment = new_gain
        except:
            self.status_bar_gaininput.setText('%.4f' % self.gain_increment)

    def mouseClicked(self, evt):
        """
        Called when the user clicks on the abs plot, used to set the fit regions.
        """
        # get the coordinates of the click
        pos = evt[0].scenePos()
        if self.plot_abs.sceneBoundingRect().contains(pos):
            mousePoint = self.plot_abs.plotItem.vb.mapSceneToView(pos)
            # check if we are adding a fit area
            if self.bAddArea.isChecked():
                # first point or second point
                if self.addFitAreaStep == 0:
                    # add a new region, first point
                    lr = pg.LinearRegionItem([mousePoint.x(),mousePoint.x()], brush=(255,240,240))
                    lr.setZValue(-10)
                    self.fitAreas.append(lr)
                    self.plot_abs.addItem(lr)
                    self.addFitAreaStep = 1
                elif self.addFitAreaStep == 1:
                    # we are setting the boundary of the first region
                    self.fitAreas[-1].setRegion([self.fitAreas[-1].getRegion()[0], mousePoint.x()])
                    self.addFitAreaStep = 0
                    self.bAddArea.setChecked(False)
                    self.bRemoveArea.setVisible(True)
                    self.bVectFit.setVisible(True)
                    self.bShowArea.setVisible(True)
            # or if we are removing an area
            elif self.bRemoveArea.isChecked():
                # loop over all areas and see if the click is inside one
                for i,lr in enumerate(self.fitAreas):
                    lims = lr.getRegion()
                    if lims[0] < mousePoint.x() < lims[1]:
                        self.fitAreas.pop(i)
                        self.plot_abs.removeItem(lr)
                        break
                self.bRemoveArea.setChecked(False) 
                if len(self.fitAreas) == 0:
                    self.bRemoveArea.setVisible(False)
                    self.bShowArea.setVisible(False)
        else:
            # not sure what would happen here...
            print('Out of bound?', pos)

    def key_press(self, key):
        """
        Move poles, zeros and gain using keyboard. This method is called by the event handler of the main window class
        """

        if self.bFitMovePZ.isChecked():
            # react to a keyboard event if the Move Poles/zeros button is active
            if self.tf_graph.selected is not None:
                # fine the pole or zero currently selected
                pz  = self.tf_graph.symbols[self.tf_graph.selected]      # 'star' for poles, 'o' for zeros
                fr  = self.tf_graph.frequencies[self.tf_graph.selected]  # frequency
                q   = self.tf_graph.Q[self.tf_graph.selected]            # Q
                idx = self.tf_graph.idx[self.tf_graph.selected]          # sequential index in the zeros or poles lists
                # start with same frequency and Q
                new_fr = fr
                new_q  = q
                # set the frequency increment as a fraction of the currently displayed range
                increment = 10**((np.log10(self.fit_fr[-1])-np.log10(self.fit_fr[0])) * self.frequency_increment)
                # set the Q increment with opposite directions for poles and zeros, so that KeyUp always move
                # the pole or zero up in the abs plot
                if pz == 'star':
                    Qincrement = 1 + self.q_increment
                else:
                    Qincrement = 1/(1 + self.q_increment)
                # change values depending on the key pressed
                if key == QtCore.Qt.Key_Right:
                    new_fr = fr * increment
                if key == QtCore.Qt.Key_Left:
                    new_fr = fr / increment
                if key == QtCore.Qt.Key_Up:
                    new_q = q * Qincrement
                if key == QtCore.Qt.Key_Down:
                    new_q = q / Qincrement
                # if there's a change, update the values in the transfer function object
                if fr != new_fr or q != new_q:
                    self.undo.put(self.tf_fit.copy())     # add current TF to the undo stack
                    if self.lock_gain_freq is not None:
                        # remember the TF gain at the lock frequency, f gain is locked
                        gain = abs(self.tf_fit.fresp(self.lock_gain_freq))
                    # update pole or zero
                    if pz == 'star':
                        self.tf_fit.p_f[idx] = new_fr
                        self.tf_fit.p_Q[idx] = new_q
                    else:
                        self.tf_fit.z_f[idx] = new_fr
                        self.tf_fit.z_Q[idx] = new_q
                    # propagate to transfer function
                    self.tf_fit.set_fQ(self.tf_fit.z_f, self.tf_fit.z_Q,
                                       self.tf_fit.p_f, self.tf_fit.p_Q, self.tf_fit.k)
                    if self.lock_gain_freq is not None:
                        # set gain if gain was locked
                        self.tf_fit.set_gain(self.lock_gain_freq, gain)
                    # replot and update graphs
                    self.plot_res.disableAutoRange()
                    self.tf_graph.updateTF(self.tf_fit)
                    self.plotMeasurements()
                    # update status line
                    if pz == 'star':
                        self.status_bar.showMessage('Selected pole fr=%f Hz, Q=%f' % (new_fr, new_q))
                    else:
                        self.status_bar.showMessage('Selected zero fr=%f Hz, Q=%f' % (new_fr, new_q))
        if self.bFitChangeGain.isChecked():
            # react to a keyboard event if the Change Gain button is active
            # start from current gain
            k = self.tf_fit.k
            new_k = k
            # change by 2% if KeyUp or 50%% if PageUp
            if key == QtCore.Qt.Key_Up:
                new_k = k * 10**(self.gain_increment/20)
            if key == QtCore.Qt.Key_Down:
                new_k = k / 10**(self.gain_increment/20)
            if key == QtCore.Qt.Key_PageUp:
                new_k = k * 1.5
            if key == QtCore.Qt.Key_PageDown:
                new_k = k / 1.5
            if k != new_k:
                # update if there is a change
                self.tf_fit.set_k(new_k)
                if self.lock_gain_value is not None:
                    self.lock_gain_value = self.lock_gain_value * new_k/k
            # update plots and graphs
            self.plot_res.disableAutoRange()
            self.tf_graph.updateTF(self.tf_fit)
            self.plotMeasurements()

    def mouseMoved(self, evt):
        """
        Called when the mouse hover over the asb plot, to move line while selecting fit regions.
        """
        # move crosshair
        pos = evt[0]
        if self.plot_abs.sceneBoundingRect().contains(pos):
            # get the mouse position and move the lines
            mousePoint = self.plot_abs.plotItem.vb.mapSceneToView(pos)
            self.vLine.setPos(mousePoint.x())
        # make crosshair visible only when adding or removing a fit area
        if self.bAddArea.isChecked() or self.bRemoveArea.isChecked():
            self.vLine.setVisible(True)
        else:
            self.vLine.setVisible(False)
        # update fit area if adding a new one
        if self.addFitAreaStep == 1:
            # we are setting the boundary of the first region
            self.fitAreas[-1].setRegion([self.fitAreas[-1].getRegion()[0], mousePoint.x()])

    def zoom(self):
        """
        Called when the user zoom in the plot area, to resample the fit frequency bins
        """
        # get the new frequency range 
        if self.fit_plotted or self.fits_plotted:
            # minimum and maximum frequencies from the view range
            fmin = 10**self.plot_abs.getViewBox().viewRange()[0][0]
            fmax = 10**self.plot_abs.getViewBox().viewRange()[0][1]
            
            # check if the range changed enough to call a resample
            current_fmin = self.fit_fr[0]
            current_fmax = self.fit_fr[-1]
            if fmin/current_fmin < 1 - self.fit_update_thr or \
                    fmin/current_fmin > 1 + self.fit_update_thr or \
                    fmax/current_fmax < 1 - self.fit_update_thr or \
                    fmax/current_fmax > 1 + self.fit_update_thr or \
                    (current_fmax - current_fmin)/(fmax - fmin) < 1 - self.fit_update_thr or \
                    (current_fmax - current_fmin)/(fmax - fmin) > 1 - self.fit_update_thr:
                # new frequency bins to span all the current range
                self.fit_fr = np.logspace(np.log10(fmin), np.log10(fmax), self.fit_nfr)
                # recompute and replot all fit curves
                if self.fit_plotted:
                    fit = self.tf_fit.fresp(self.fit_fr)
                    self.line_fit_abs.setData(self.fit_fr, np.abs(fit))
                    self.line_fit_phi.setData(self.fit_fr, 180/np.pi*np.angle(fit))
                if self.fits_plotted:
                    for o in self.fits.keys():
                        self.fits[o]['fit'] = (np.polyval(self.fits[o]['tf_fit'].num, 2j*np.pi*self.fit_fr) /
                                               np.polyval(self.fits[o]['tf_fit'].den, 2j*np.pi*self.fit_fr))
                        self.line_fits_abs[o].setData(self.fit_fr, np.abs(self.fits[o]['fit']))
                        self.line_fits_phi[o].setData(self.fit_fr, 180/np.pi*np.angle(self.fits[o]['fit']))
                if len(self.reference_fits):
                    # update reference fits
                    for i, (r, q) in enumerate(zip(self.reference_fits, self.reference_items)):
                        # recompute fit and residual
                        fit = r.fresp(self.fit_fr)
                        res = abs((r.fresp(self.fr) - self.tf) / self.tf)
                        # plot them
                        self.reference_plot_abs[i].setData(self.fit_fr, np.abs(fit))
                        self.reference_plot_phi[i].setData(self.fit_fr, 180/np.pi*np.angle(fit))
                        self.reference_plot_res[i].setData(self.fr, res)
        else:
            # if we haven't plotted a fit yet, this is called at the first plot, so we set the initial range
            # to match the transfer function data
            self.fit_fr = np.logspace(np.log10(self.fr[1]), np.log10(self.fr[-1]), self.fit_nfr)

    def show_areas(self):
        """
        Toggle the visibility of fit areas, called when the Show Fit Areas button is toggled
        """
        for a in self.fitAreas:
            a.setVisible(self.bShowArea.isChecked())
            
    def plotMeasurements(self):
        '''
        Replot measurements and fit curves
        '''

        # plot the measurement above coherence
        coh_mask = np.ones_like(self.fr)
        coh_mask[self.co > self.coh_hLine.getPos()[1]] = np.nan
        if self.line_meas_abs1 is None:
            # new plot line
            self.line_meas_abs1 = self.plot_abs.plot(self.fr, coh_mask*np.abs(self.tf),
                                                     pen=pg.mkPen((200,200,255), width=1))
            self.line_meas_phi1 = self.plot_phi.plot(self.fr, coh_mask*180/np.pi*np.angle(self.tf),
                                                     pen=pg.mkPen((200,200,255), width=1))
        else:
            # existing plot line, just update data
            self.line_meas_abs1.setData(self.fr, coh_mask*np.abs(self.tf))
            self.line_meas_phi1.setData(self.fr, coh_mask*180/np.pi*np.angle(self.tf))
        # plot the measurement below coherence
        coh_mask = np.ones_like(self.fr)
        coh_mask[self.co <= self.coh_hLine.getPos()[1]] = np.nan
        if self.line_meas_abs2 is None:
            # new plot line
            self.line_meas_abs2 = self.plot_abs.plot(self.fr, coh_mask*np.abs(self.tf),
                                                     pen=pg.mkPen('b', width=2))
            self.line_meas_phi2 = self.plot_phi.plot(self.fr, coh_mask*180/np.pi*np.angle(self.tf),
                                                     pen=pg.mkPen('b', width=2))
        else:
            # existing plot line, just update data
            self.line_meas_abs2.setData(self.fr, coh_mask*np.abs(self.tf))
            self.line_meas_phi2.setData(self.fr, coh_mask*180/np.pi*np.angle(self.tf))
        # plot coherence curve
        if self.line_meas_coh is None:
            self.line_meas_coh = self.plot_coh.plot(self.fr, self.co, pen=pg.mkPen('b', width=2))
        else:
            self.line_meas_coh.setData(self.fr, self.co)

        if self.tf_fit is not None:
            # get sampled version of current fit
            fit = self.tf_fit.fresp(self.fit_fr)
            if self.line_fit_abs is None:
                # new plot line, using a custom class defined below, to handle mouse drag
                self.line_fit_abs = CustomPlotItem(self, self.fit_fr, np.abs(fit),
                                                   pen=pg.mkPen('r', width=1))
                self.plot_abs.addItem(self.line_fit_abs)
                self.line_fit_phi = self.plot_phi.plot(self.fit_fr, 180/np.pi*np.angle(fit),
                                                       pen=pg.mkPen('r', width=1))
            else:
                # existing plot line, just update data
                self.line_fit_abs.setData(self.fit_fr, np.abs(fit))
                self.line_fit_phi.setData(self.fit_fr, 180/np.pi*np.angle(fit))
            # plot residual fit
            res = abs((self.tf_fit.fresp(self.fr) - self.tf)/self.tf)
            if self.line_meas_res is None:
                # new plot line
                self.line_meas_res = self.plot_res.plot(self.fr, res, pen=pg.mkPen('r', width=2))
            else:
                # existing plot line
                self.line_meas_res.setData(self.fr, res)
            self.fit_plotted = True
            # connect methods to handle clicks on the fit line
            self.proxy_line_fit_click = pg.SignalProxy(self.line_fit_abs.sigClicked,
                                                       rateLimit=10, slot=self.click_fit_curve)
            # curve is clickable only if the proper buttons are selected
            self.line_fit_abs.curve.setClickable(self.bFitAddCmpxPole.isChecked() or
                                                 self.bFitAddRealPole.isChecked() or
                                                 self.bFitAddCmpxZero.isChecked() or
                                                 self.bFitAddRealZero.isChecked())

        # plot reference fits
        if len(self.reference_fits):
            # remove all previous traces, if any
            for a,p,r in zip(self.reference_plot_abs, self.reference_plot_phi, self.reference_plot_res):
                self.plot_abs.removeItem(a)
                self.plot_phi.removeItem(p)
                self.plot_res.removeItem(r)
            self.reference_plot_abs = []
            self.reference_plot_phi = []
            self.reference_plot_res = []
            # add again legend if needed
            if self.legend_abs is None:
                self.legend_abs = self.plot_abs.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.legend_phi = self.plot_phi.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
                self.legend_res = self.plot_res.getPlotItem().addLegend((80, -60), colCount=2, brush='w')
            # replot all active references
            for i,(r,q) in enumerate(zip(self.reference_fits, self.reference_items)):
                # recompute fit and residual
                fit = r.fresp(self.fit_fr)
                res = abs((r.fresp(self.fr) - self.tf) / self.tf)
                # plot them
                self.reference_plot_abs.append(self.plot_abs.plot(self.fit_fr, np.abs(fit),
                                                pen=pg.intColor(i+1), name=q.text()))
                self.reference_plot_phi.append(self.plot_phi.plot(self.fit_fr, 180/np.pi*np.angle(fit),
                                                pen=pg.intColor(i+1), name=q.text()))
                self.reference_plot_res.append(self.plot_res.plot(self.fr, res,
                                                pen=pg.intColor(i+1), name=q.text()))
                # make it visible only if checked
                if q.isChecked():
                    self.reference_plot_abs[-1].show()
                    self.reference_plot_phi[-1].show()
                    self.reference_plot_res[-1].show()
                else:
                    self.reference_plot_abs[-1].hide()
                    self.reference_plot_phi[-1].hide()
                    self.reference_plot_res[-1].hide()

        # add a marker where the fit gain is locked
        if self.lock_gain_freq is not None:
            if self.lock_gain_dot is None:
                self.lock_gain_dot = self.plot_abs.plot([self.lock_gain_freq], [self.lock_gain_value],
                                                        symbol='d', symbolPen='k', symbolBrush='k',
                                                        symbolSize=10)
            else:
                self.lock_gain_dot.setData([self.lock_gain_freq], [self.lock_gain_value])

    def click_fits_curve(self, ev, click):
        '''
        React to a click on one of the fit curves, called when the user is picking one of the fits
        '''

        if self.bChooseFit.isChecked():
            # find the fit order and save the results
            o = int(ev.name()[4:].split(' ')[0])
            #self.tf_fit = TF(z=self.fits[o]['z'], p=self.fits[o]['p'], k=self.fits[o]['k'])
            self.tf_fit = self.fits[o]['tf_fit']
            self.tf_graph = TranferFunctionGraph(self.tf_fit, self)
            self.plot_abs.addItem(self.tf_graph)
            # remove all fits
            self.clear_fits()
            # replot measurements (and fit)
            self.plotMeasurements()
            self.bChooseFit.setChecked(False)
            self.fit_plotted = True
            # fit selected, activate and deactivate the proper buttons
            self.bClearFit.setVisible(False)
            self.bChooseFit.setVisible(False)
            self.bShowArea.setChecked(False)
            self.bAddArea.setVisible(True)
            self.bRemoveArea.setVisible(True)
            self.bEditFit.setVisible(True)
            self.bVectFit.setVisible(False)
            self.bExportFit.setVisible(True)
            self.app.restoreOverrideCursor()
            
            
    def click_fit_curve(self, ev):#, click):
        '''
        React to a click on the fit curve, to add poles and zeros or to set where the gain should be locked
        '''
        ev, click = ev
        if self.bFitAddCmpxPole.isChecked():
            # add current tf to undo stack
            self.undo.put(self.tf_fit.copy())
            if self.lock_gain_freq is None:
                # add a pole maintaining the gain at DC, with Q=1/sqrt(2)
                self.tf_fit.add_pole_fQ(10**click.pos().x(), 1/np.sqrt(2))
            else:
                # add a pole maintaining the gain at the lock frequency
                gain = np.abs(self.tf_fit.fresp(self.lock_gain_freq))
                self.tf_fit.add_pole_fQ(10 ** click.pos().x(), 1 / np.sqrt(2))
                self.tf_fit.set_gain(self.lock_gain_freq, gain)
        if self.bFitAddCmpxZero.isChecked():
            # add current tf to undo stack
            self.undo.put(self.tf_fit.copy())
            if self.lock_gain_freq is None:
                # add a zero maintaining the gain at DC, with Q=1/sqrt(2)
                self.tf_fit.add_zero_fQ(10**click.pos().x(), 1/np.sqrt(2))
            else:
                # add a zero maintaining the gain at the lock frequency
                gain = np.abs(self.tf_fit.fresp(self.lock_gain_freq))
                self.tf_fit.add_zero_fQ(10**click.pos().x(), 1/np.sqrt(2))
                self.tf_fit.set_gain(self.lock_gain_freq, gain)
        if self.bFitAddRealPole.isChecked():
            # add current tf to undo stack
            self.undo.put(self.tf_fit.copy())
            if self.lock_gain_freq is None:
                # add a pole maintaining the gain at DC
                self.tf_fit.add_pole_fQ(10**click.pos().x(), 0)
            else:
                # add a pole maintaining the gain at the lock frequency
                gain = np.abs(self.tf_fit.fresp(self.lock_gain_freq))
                self.tf_fit.add_pole_fQ(10**click.pos().x(), 0)
                self.tf_fit.set_gain(self.lock_gain_freq, gain)
        if self.bFitAddRealZero.isChecked():
            # add current tf to undo stack
            self.undo.put(self.tf_fit.copy())
            if self.lock_gain_freq is None:
                # add a zero maintaining the gain at DC
                self.tf_fit.add_zero_fQ(10**click.pos().x(), 0)
            else:
                # add a zero maintaining the gain at the lock frequency
                gain = np.abs(self.tf_fit.fresp(self.lock_gain_freq))
                self.tf_fit.add_zero_fQ(10**click.pos().x(), 0)
                self.tf_fit.set_gain(self.lock_gain_freq, gain)
        if self.bFitLockGain.isChecked() and self.lock_gain_freq is None:
            # if the lock gain button is selected and we don't have a lock gain frequency, add itt
            self.lock_gain_freq = 10**click.pos().x()
            self.lock_gain_value = abs(self.tf_fit.fresp(self.lock_gain_freq))
        # update all plots and graphs
        self.tf_graph.updateTF(self.tf_fit)
        self.plotMeasurements()

    def edit_fit(self):
        '''
        Toggle fit edit toolbar
        '''
        if self.bEditFit.isChecked():
            self.edit_toolbar.show()
        else:
            self.edit_toolbar.hide()
   
    def plotFits(self):
        """
        Plot all fits resulting from VectFit
        """
        # diictionaries to contain the plots
        self.line_fits_abs = {}
        self.line_fits_phi = {}
        self.line_fits_res = {}
        # add legends
        self.legend_abs = self.plot_abs.getPlotItem().addLegend((80,60), colCount=2, brush='w')
        self.legend_phi = self.plot_phi.getPlotItem().addLegend((80,60), colCount=2, brush='w')
        self.legend_res = self.plot_res.getPlotItem().addLegend((80,60), colCount=2, brush='w')
        # get current view limits, to avoid strange zoom behaviors
        fmin, fmax = self.plot_abs.getViewBox().viewRange()[0][0], self.plot_abs.getViewBox().viewRange()[0][1]

        for i,o in enumerate(self.fits.keys()):
            # legend text
            name='Fit %d' % o
            if self.fits[o]['tf_fit'].delay != 0:
                name += ' (delay=%.3es' % self.fits[o]['tf_fit'].delay
                if self.fits[o]['tf_fit'].delay_approx_pade:
                    name += ' Padé approx.)'
                else:
                    name += ')'

            # add all fits
            self.line_fits_abs[o] = self.plot_abs.plot(self.fit_fr, np.abs(self.fits[o]['fit']),
                                                       pen=pg.intColor(i), name=name)
            # connect method to click signal
            self.line_fits_abs[o].sigClicked.connect(self.click_fits_curve)
            self.line_fits_abs[o].curve.setClickable(True)
            self.line_fits_phi[o] = self.plot_phi.plot(self.fit_fr, 180/np.pi*np.angle(self.fits[o]['fit']),
                                                       pen=pg.intColor(i), name=name)
            self.line_fits_phi[o].sigClicked.connect(self.click_fits_curve)
            self.line_fits_phi[o].curve.setClickable(True)
            self.line_fits_res[o] = self.plot_res.plot(self.fr, np.abs(self.fits[o]['res']),
                                                       pen=pg.intColor(i), name=name)
            self.line_fits_res[o].sigClicked.connect(self.click_fits_curve)
            self.line_fits_res[o].curve.setClickable(True)
            # set the x range to void weird zoom effects
            self.plot_abs.setXRange(fmin, fmax)
        self.fits_plotted = True
   
    def run_vectfit(self):
        """
        Run vectfit to fit the transfer function
        """
        
        # remove existing fit if any
        self.fit = None
        self.plotMeasurements()
        
        # open a dialog to choose fit order
        # order, ok = QtWidgets.QInputDialog.getText(self.win, 'Select fit order(s)', 
        #     "Select the order of the fit. You can write a single number, a list, or a valid python expression\n" +
        #     "that return a list of values, for example 'np.arange(2,21,2)'  to use all even orders between\n" +
        #     "2 and 20, with steps of 2; or or '8,10,12' to specify a list of orders.")
        
        # open a dialog to choose fit order
        dialog = FitDialog()
        dialog.exec()

        if dialog.ok:
            # evaluate the fit orders
            order = dialog.order.text()
            if order == '':
                return
            orders = eval(order)
            if type(orders) is float or type(orders) is int:
                orders = [int(orders)]

            # extract fit frequencies from the ranges
            self.fit_idx = np.zeros_like(self.fr, dtype=bool)
            for i,lr in enumerate(self.fitAreas):
                fmin, fmax = lr.getRegion()
                fmin = 10**fmin
                fmax = 10**fmax
                self.fit_idx[(self.fr>fmin)*(self.fr<fmax)] = True
                
            # apply coherence mask
            coh_thr = self.coh_hLine.getPos()[1]
            self.fit_idx[self.co<coh_thr] = False
                
            # check if we need to estimate the delay
            if dialog.delay_check.isChecked():
                delay_lim = [float(dialog.dmin.text()), float(dialog.dmax.text())]

            # run vectfit
            self.fits = {}
            if self.fit_fr is None:
                self.fit_fr = np.logspace(np.log10(self.fr[1]), np.log10(self.fr[-1]), self.fit_nfr)
            # once for each order
            for o in orders:
                if dialog.delay_check.isChecked():
                    z,p,k,delay,n,d = vectfit3.vectfit3_zpk_delay(self.fr[self.fit_idx], self.tf[self.fit_idx], o,
                                                    delay_lim, 1/abs(self.tf[self.fit_idx]),
                                                    allow_rightplane_poles=False, n_iter=10)
                    # compute response of fits and residuals
                    if dialog.delay_pade.isChecked():
                        tf_fit = TF(z, p, k, delay=delay, pade_order=3)
                    else:
                        tf_fit = TF(z, p, k, delay=delay, pade_order=0)
                    #fit = np.polyval(n, 2j*np.pi*self.fr) / np.polyval(d, 2j*np.pi*self.fr)
                    res = abs(self.tf - tf_fit.fresp(self.fr))/abs(self.tf)
                    fit = tf_fit.fresp(self.fit_fr)
                    #fit = np.polyval(n, 2j*np.pi*self.fit_fr) / np.polyval(d, 2j*np.pi*self.fit_fr)
                    # save in dictionary
                    #self.fits[o] = dict(zip(['z', 'p', 'k', 'num', 'den', 'fit', 'res'], [z,p,k,n,d,fit,res]))
                    self.fits[o] = dict(zip(['tf_fit', 'fit', 'res'], [tf_fit, fit, res]))
                else:
                    z,p,k,n,d = vectfit3.vectfit3_zpk(self.fr[self.fit_idx], self.tf[self.fit_idx], o,
                                                    1/abs(self.tf[self.fit_idx]),
                                                    allow_rightplane_poles=False, n_iter=10)
                    # compute response of fits and residuals
                    tf_fit = TF(z, p, k)
                    res = abs(self.tf - tf_fit.fresp(self.fr))/abs(self.tf)
                    fit = tf_fit.fresp(self.fit_fr)
                    #fit = np.polyval(n, 2j*np.pi*self.fr) / np.polyval(d, 2j*np.pi*self.fr)
                    #res = abs(self.tf - fit)/abs(self.tf)
                    #fit = np.polyval(n, 2j*np.pi*self.fit_fr) / np.polyval(d, 2j*np.pi*self.fit_fr)
                    # save in dictionary
                    #self.fits[o] = dict(zip(['z', 'p', 'k', 'num', 'den', 'fit', 'res'], [z,p,k,n,d,fit,res]))
                    self.fits[o] = dict(zip(['tf_fit', 'fit', 'res'], [tf_fit, fit, res]))
                    
            # plot all fits
            self.plotFits()
            # enable fit editing buttons and toolbars
            self.bClearFit.setVisible(True)
            self.bChooseFit.setVisible(True)
            self.bShowArea.setChecked(False)

    def leastsquare_optimization(self):
        """
        Open a new dialog window to set optimization parameters
        """
        # check if fit areas are defined
        if len(self.fitAreas) == 0:
            # no fit area, show error message
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Critical)
            msg.setText("Must define at least one fit area before running an optimization.")
            msg.setWindowTitle("Error")
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
            msg.exec()
        else:
            # create and display dialog
            if self.opt_gui is None:
                self.opt_gui = OptimizationGUI(self)
            self.opt_gui.show()
            # wait it for window to be closed and optimization concluded
            loop = QtCore.QEventLoop()
            self.opt_gui.opt_done.connect(loop.quit)
            loop.exec()


    def clear_fits(self):
        """
        Remove all fit curves and legends
        """
        self.fits_plotted = False
        self.plot_abs.getPlotItem().removeItem(self.legend_abs)
        self.plot_phi.getPlotItem().removeItem(self.legend_phi)
        self.plot_res.getPlotItem().removeItem(self.legend_res)
        for o in self.fits.keys():
            self.plot_abs.removeItem(self.line_fits_abs[o])
            self.plot_phi.removeItem(self.line_fits_phi[o])
            self.plot_res.removeItem(self.line_fits_res[o])
        self.fits = None
        self.bShowArea.setChecked(True)
        self.bChooseFit.setVisible(False)
        self.bClearFit.setVisible(False)

    def export_fit(self):
        """
        Open a dialog and print the foton command string and ZPK
        """

        # Create a new window with grid layout
        q = QtWidgets.QWidget()
        q.resize(800,600)
        g = QtWidgets.QGridLayout()
        q.setLayout(g)

        # one combo boxes to select what to export
        v = QtWidgets.QHBoxLayout()
        g.addLayout(v, 1, 1)
        label1 = QtWidgets.QLabel('Show fit')
        v.addWidget(label1)
        combo1 = QtWidgets.QComboBox()
        combo1.addItem('Current')
        for i in self.reference_items:
            combo1.addItem(i.text())
        combo1.setCurrentIndex(0)
        v.addWidget(combo1)

        # text boxes
        label3 = QtWidgets.QLabel('Foton design string')
        g.addWidget(label3, 2, 1)
        label4 = QtWidgets.QLabel('ZPK description')
        g.addWidget(label4, 4, 1)
        txt_foton = QtWidgets.QTextEdit()
        txt_foton.setFont(pg.QtGui.QFont('Courier'))
        txt_zpk = QtWidgets.QTextEdit()
        txt_zpk.setFont(pg.QtGui.QFont('Courier'))
        g.addWidget(txt_foton, 3, 1)
        g.addWidget(txt_zpk, 5, 1)

        # buttons
        done_button = QtWidgets.QPushButton("Done", q)
        g.addWidget(done_button, 6, 1)
        done_button.clicked.connect(q.close)

        # function to update text boxes
        def update():
            # find the controllers to be divided out
            idx1 = combo1.currentIndex()

            if idx1 == 0:
                fit = self.tf_fit.copy()
            else:
                fit = self.reference_fit[idx1-1].copy()

            try:
                fs = float(fs_finput.text())
            except:
                fs = 16384
                fs_finput.setText('16384')

            # generate foton string
            try:
                import foton
                filt = foton.FilterDesign(fs)
                res = filt.set_zpk(fit.z, fit.p, fit.k)
                if res:
                    if fit.delay != 0 and not fit.delay_approx_pade:
                        txt_foton.setText(filt.design + '\n\n!! Delay that cannot be added to foton design = %e s' % fit.delay)
                    else:
                        txt_foton.setText(filt.design)
                else:
                    txt_foton.setText('foton did not return a valid design string')
            except:
                # no foton libray found
                txt_foton.setText('Failed to import foton libary')

            # generate ZPK description
            s = "\nLaplace domain:\n\n"
            s += 'Z = \n' + np.array2string(fit.z, separator=',') + '\n'
            s += 'P = \n' + np.array2string(fit.p, separator=',') + '\n'
            s += 'K = \n' + str(fit.k) + '\n'
            if fit.delay != 0 and not fit.delay_approx_pade:
                s += 'Delay = %e s\n' % fit.delay
            s += "\nFrequency domain [Hz]:\n"
            s += "Gain: %8.3e\n" % fit.k
            s += "Poles\t     f\t       Q\n"
            s += "-----------------------------------\n"
            for f, q in zip(fit.p_f, fit.p_Q):
                s += "\t%8.3f\t%8.3f\n" % (f, q)
            s += "Zeros\t     f\t       Q\n"
            s += "-----------------------------------\n"
            for f, q in zip(fit.z_f, fit.z_Q):
                s += "\t%8.3f\t%8.3f\n" % (f, q)
            if fit.delay != 0 and not fit.delay_approx_pade:
                s += 'Delay = %e s\n' % fit.delay
            txt_zpk.setText(s)

        # text box to set sampling frequency
        label5 = QtWidgets.QLabel('Sampling frequency [Hz]')
        v.addWidget(label5)
        fs_finput = QtWidgets.QLineEdit()
        fs_finput.setText('16384')
        fs_finput.setFixedWidth(70)
        fs_finput.editingFinished.connect(update)
        v.addWidget(fs_finput)
        v.addStretch()

        # update text fields
        update()
        # connect update to changes
        combo1.activated.connect(update)

        # open the window and wait for it to close
        q.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        q.show()
        loop = QtCore.QEventLoop()
        q.destroyed.connect(loop.quit)
        loop.exec()

    def print_fit(self):
        """
        Print the fit to terminal
        """
        print("\nLaplace domain:\n")
        print('Z = \n' + np.array2string(self.tf_fit.z, separator=','))
        print('P = \n' + np.array2string(self.tf_fit.p, separator=','))
        print('K = \n' + str(self.tf_fit.k))
        print("\nFrequency domain [Hz]:")
        print("Gain: %8.3e" % self.tf_fit.k)
        print("Poles\t     f\t            Q")
        print("-----------------------------------")
        for f, q in zip(self.tf_fit.p_f, self.tf_fit.p_Q):
            print("\t%8.3f\t%8.3f" % (f, q))
        print("Zeros\t     f\t            Q")
        print("-----------------------------------")
        for f, q in zip(self.tf_fit.z_f, self.tf_fit.z_Q):
            print("\t%8.3f\t%8.3f" % (f, q))

    def edit_zpk(self):
        """
        Open a new window with editable lists of pole and zero frequencies and Qs
        """

        # work on a copy of the transfer function
        tf = self.tf_fit.copy()

        # Create a new window with grid layout
        q = QtWidgets.QWidget()
        g = QtWidgets.QGridLayout()
        q.setLayout(g)

        # make four lists and link scrollbars between frequency and Q, and labels
        list_zeros_f = QtWidgets.QListWidget(q)
        list_zeros_f.setAlternatingRowColors(True)
        list_zeros_q = QtWidgets.QListWidget(q)
        list_zeros_q.setAlternatingRowColors(True)
        label_zeros_f = QtWidgets.QLabel('Zero Frequencies [Hz]')
        label_zeros_q = QtWidgets.QLabel('Zero Qs')
        list_zeros_f.verticalScrollBar().valueChanged.connect(list_zeros_q.verticalScrollBar().setValue)
        list_zeros_q.verticalScrollBar().valueChanged.connect(list_zeros_f.verticalScrollBar().setValue)
        list_poles_f = QtWidgets.QListWidget(q)
        list_poles_f.setAlternatingRowColors(True)
        list_poles_q = QtWidgets.QListWidget(q)
        list_poles_q.setAlternatingRowColors(True)
        label_poles_f = QtWidgets.QLabel('Pole Frequencies [Hz]')
        label_poles_q = QtWidgets.QLabel('Pole Qs')
        list_poles_f.verticalScrollBar().valueChanged.connect(list_poles_q.verticalScrollBar().setValue)
        list_poles_q.verticalScrollBar().valueChanged.connect(list_poles_f.verticalScrollBar().setValue)
        g.addWidget(list_zeros_f, 1, 0)
        g.addWidget(list_zeros_q, 1, 1)
        g.addWidget(list_poles_f, 3, 0)
        g.addWidget(list_poles_q, 3, 1)
        g.addWidget(label_zeros_f, 0, 0)
        g.addWidget(label_zeros_q, 0, 1)
        g.addWidget(label_poles_f, 2, 0)
        g.addWidget(label_poles_q, 2, 1)
        # link selections between frequency and Q
        list_zeros_f.currentRowChanged.connect(list_zeros_q.setCurrentRow)
        list_poles_f.currentRowChanged.connect(list_poles_q.setCurrentRow)
        list_zeros_q.currentRowChanged.connect(list_zeros_f.setCurrentRow)
        list_poles_q.currentRowChanged.connect(list_poles_f.setCurrentRow)

        # add text input for gain
        def gain_change(x):
            """
            Interpreter the gain input as float adn set TF gain accordingly
            """
            try:
                tf.k = float(x)
            except:
                pass
        label_gain = QtWidgets.QLabel('Gain (k)')
        #label_gain.setAlignment(QtCore.Qt.AlignRight)
        g.addWidget(label_gain, 4, 0)
        gain = QtWidgets.QLineEdit()
        gain.setValidator(pg.QtGui.QDoubleValidator())
        gain.textChanged.connect(gain_change)
        gain.setText(str(tf.k))
        g.addWidget(gain, 5, 0)

        if tf.delay != 0 and not tf.delay_approx_pade:
            def delay_change(x):
                """
                Interpreter the delay input as float and set TF delay accordingly
                """
                tf.delay = float(x)

            label_delay = QtWidgets.QLabel('Delay [s]')
            #label_gain.setAlignment(QtCore.Qt.AlignRight)
            g.addWidget(label_delay, 4, 1)
            delay = QtWidgets.QLineEdit()
            delay.setValidator(pg.QtGui.QDoubleValidator())
            delay.textChanged.connect(delay_change)
            delay.setText(str(tf.delay))
            g.addWidget(delay, 5, 1)

        # add buttons at the bottom
        button_done = QtWidgets.QPushButton("Done", q)
        button_cancel = QtWidgets.QPushButton("Cancel", q)
        g.addWidget(button_done, 6, 0)
        g.addWidget(button_cancel, 6, 1)

        # buttons to add / remove zero
        button_zero_layout = QtWidgets.QVBoxLayout()
        button_delete_zero = QtWidgets.QPushButton("Delete", q)
        button_add_zero = QtWidgets.QPushButton("Add", q)
        button_zero_layout.addWidget(button_delete_zero)
        button_zero_layout.addWidget(button_add_zero)
        button_zero_layout.addStretch()
        g.addLayout(button_zero_layout, 1,2)

        def add_zero():
            """
            Method called when the Add zero button is clicked
            """
            # Add at the bottom, f=0, Q=0
            list_zeros_f.addItem('0.0' % f)
            list_zeros_q.addItem('0.0' % Q)
            # select added item and make it editable
            list_zeros_f.setCurrentRow(list_zeros_f.count() - 1)
            item = list_zeros_f.currentItem()
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
            item = list_zeros_q.currentItem()
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
            # add to transfer function
            tf.z_f = np.append(tf.z_f, 0)
            tf.z_Q = np.append(tf.z_Q, 0)
        # connect method to button click
        button_add_zero.clicked.connect(add_zero)
        def delete_zero():
            """
            Method called when the Delete buttton is clicked
            """
            # find current item and remove it
            i = list_zeros_f.currentRow()
            list_zeros_f.takeItem(i)
            list_zeros_q.takeItem(i)
            # remove from transfer function
            tf.z_f = np.delete(tf.z_f, i)
            tf.z_Q = np.delete(tf.z_Q, i)
        # connect method to button click
        button_delete_zero.clicked.connect(delete_zero)

        # buttons to add / remove pole
        button_pole_layout = QtWidgets.QVBoxLayout()
        button_delete_pole = QtWidgets.QPushButton("Delete", q)
        button_add_pole = QtWidgets.QPushButton("Add", q)
        button_pole_layout.addWidget(button_delete_pole)
        button_pole_layout.addWidget(button_add_pole)
        button_pole_layout.addStretch()
        g.addLayout(button_pole_layout, 3,2)

        def add_pole():
            """
                   Method called when the Add pole button is clicked
            """
            # Add at the bottom, f=0, Q=0
            list_poles_f.addItem('0.0' % f)
            list_poles_q.addItem('0.0' % f)
            # select added item and make it editable
            list_poles_f.setCurrentRow(list_poles_f.count()-1)
            item = list_poles_f.currentItem()
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
            item = list_poles_q.currentItem()
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
            # add to transfer function
            tf.p_f = np.append(tf.p_f, 0)
            tf.p_Q = np.append(tf.p_Q, 0)
        # connect method to button click
        button_add_pole.clicked.connect(add_pole)
        def delete_pole():
            """
            Method called when the Delet buttton is clicked
            """
            # find current item and remove it
            i = list_poles_f.currentRow()
            list_poles_f.takeItem(i)
            list_poles_q.takeItem(i)
            # remove from transfer function
            tf.p_f = np.delete(tf.p_f, i)
            tf.p_Q = np.delete(tf.p_Q, i)
        # connect method to button click
        button_delete_pole.clicked.connect(delete_pole)

        # fill lists with values from the transfer function
        for f, Q in zip(tf.p_f, tf.p_Q):
            list_poles_f.addItem('%.3f' % f)
            list_poles_q.addItem('%.3f' % Q)
        for f, Q in zip(tf.z_f, tf.z_Q):
            list_zeros_f.addItem('%.3f' % f)
            list_zeros_q.addItem('%.3f' % Q)
        # set initial selection for poles and zeros
        list_zeros_q.setCurrentRow(0)
        list_poles_q.setCurrentRow(0)

        # make all items editable
        for i in range(list_poles_f.count()):
            item = list_poles_f.item(i)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
        for i in range(list_poles_q.count()):
            item = list_poles_q.item(i)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
        for i in range(list_zeros_f.count()):
            item = list_zeros_f.item(i)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
        for i in range(list_zeros_q.count()):
            item = list_zeros_q.item(i)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)

        # methods to handle changes in the poles and zeros
        def modified_pole_f(ev):
            try:
                # interpreter as float
                f = float(ev.text())
                if f >= 0:
                    # update pole frequency only if frequency is >0
                    tf.p_f[list_poles_f.currentRow()] = f
                else:
                    # wrong frequency, reset to previous value
                    list_poles_f.currentItem().setText('%.3f' % tf.p_f[list_poles_f.currentRow()])
            except:
                # cannot interpreter as float, reset to previous value
                list_poles_f.currentItem().setText('%.3f' % tf.p_f[list_poles_f.currentRow()])
        def modified_pole_q(ev):
            try:
                # interpreter as float
                q = float(ev.text())
                if q >= 0:
                    # update pole Q only if Q is >= 0
                    tf.p_Q[list_poles_q.currentRow()] = q
                else:
                    # wrong Q, reset to previous value
                    list_poles_q.currentItem().setText('%.3f' % tf.p_Q[list_poles_q.currentRow()])
            except:
                # cannot interpreter as float, reset to previous value
                list_poles_q.currentItem().setText('%.3f' % tf.p_Q[list_poles_q.currentRow()])
        def modified_zero_f(ev):
            try:
                # interpreter as float
                f = float(ev.text())
                if f * tf.z_f[list_zeros_f.currentRow()] < 0 and tf.z_Q[list_zeros_f.currentRow()] == 0 :
                    # flip gain if the zero frequency changes sign
                    tf.k = -tf.k
                    gain.setText(str(tf.k))
                tf.z_f[list_zeros_f.currentRow()] = f
            except:
                # cannot interpreter as float, reset to previous value
                list_zeros_f.currentItem().setText('%.3f' % tf.z_f[list_zeros_f.currentRow()])
        def modified_zero_q(ev):
            try:
                # interpreter as float
                q = float(ev.text())
                if q >= 0:
                    # update zero Q only if Q is >= 0
                    tf.z_Q[list_zeros_q.currentRow()] = q
                else:
                    # wrong Q, reset to previous value
                    list_zeros_q.currentItem().setText('%.3f' % tf.z_Q[list_zeros_q.currentRow()])
            except:
                # cannot interpreter as float, reset to previous value
                list_zeros_q.currentItem().setText('%.3f' % tf.z_Q[list_zeros_q.currentRow()])
        # connect methods too item changes
        list_poles_f.itemDelegate().closeEditor.connect(modified_pole_f)
        list_poles_q.itemDelegate().closeEditor.connect(modified_pole_q)
        list_zeros_f.itemDelegate().closeEditor.connect(modified_zero_f)
        list_zeros_q.itemDelegate().closeEditor.connect(modified_zero_q)

        # meetod to react to click on buttons
        def click_done():
            """
            Called when Done is clicked, update the transfer function
            """
            # add current TF too undo stack
            self.undo.put(self.tf_fit.copy())
            # remember gain if lock gain is set
            if self.lock_gain_freq is not None:
                gain = abs(self.tf_fit.fresp(self.lock_gain_freq))
            # set the new z,p,k
            self.tf_fit.set_fQ(tf.z_f, tf.z_Q, tf.p_f, tf.p_Q, tf.k)
            # set delay if there's any
            if self.tf_fit.delay != 0 and not self.tf_fit.delay_approx_pade:
                self.tf_fit.delay = tf.delay
            # set gain if lock gain is set
            if self.lock_gain_freq is not None:
                self.tf_fit.set_gain(self.lock_gain_freq, gain)
            # update all plots and graphs
            self.tf_graph.updateTF(self.tf_fit)
            self.plotMeasurements()
            q.close()
        def click_cancel():
            """
            Called when clicking on Cancel, just close window
            """
            q.close()
        # connect methods
        button_done.clicked.connect(click_done)
        button_cancel.clicked.connect(click_cancel)

        # open the window and wait for it to close
        q.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        q.show()
        loop = QtCore.QEventLoop()
        q.destroyed.connect(loop.quit)
        loop.exec()

########################################################################################################################

class WindowKeyEvent(QtWidgets.QMainWindow):
    """
    Class to capture keypress events and pass them to the main application class
    """
    def __init__(self, parent):
        super().__init__()
        # remember parent InteractiveFitting class to pass along keys
        self.parent = parent

    def keyPressEvent(self, event):
        """
        Called when a key is pressed, pass keystroke along to main class
        """
        if isinstance(event, pg.QtGui.QKeyEvent):
            self.parent.key_press(event.key())

########################################################################################################################

class TranferFunctionGraph(pg.GraphItem):
    '''
    Class to handle plots of movable poles and zeros
    '''
    # based on https://github.com/pyqtgraph/pyqtgraph/blob/develop/examples/CustomGraphItem.py
    def __init__(self, tf, parent):
        # keep track of which point is beingg dragged
        self.dragPoint = None
        # initial offset between click position and point position
        self.dragOffset = None
        # keep track of selected point
        self.selected = None
        # transfer function
        self.tf = tf
        # remmeber parent class
        self.parent = parent
        # call class init
        pg.GraphItem.__init__(self)
        # connect click event
        self.scatter.sigClicked.connect(self.clicked)
        # update data from transfer function
        self.setData()

        
    def updateTF(self, tf):
        """
        Update points from transfer function tf
        """
        self.tf = tf
        self.setData()    
    
    def setData(self):
        """
        Update point coordinates from transfer function poles and zeros
        """
        # list of all frequencies (poles then zeros)
        self.frequencies = np.r_[self.tf.p_f, self.tf.z_f]
        # list of all Qs (poles then zeros)
        self.Q = np.r_[self.tf.p_Q, self.tf.z_Q]
        # list of all indexes in the pole and zeros lists (poles then zeros)
        self.idx = np.r_[np.arange(self.tf.p_f.shape[0]), np.arange(self.tf.z_f.shape[0])]
        # symbols ('star' for poles and 'o' for zeros
        self.symbols = np.array(['star']*self.tf.p_f.shape[0] + ['o']*self.tf.z_f.shape[0])
        # y positions of all points
        self.vals = np.abs(self.tf.fresp(self.frequencies))
        self.pos = np.c_[np.log10(np.abs(self.frequencies)), np.log10(self.vals)]
        # set pens brushes and sizes
        self.pens = []
        self.brushes = []
        self.sizes = []
        # empty symbols for real, solid symbols for complex
        for f,q in zip(self.frequencies, self.Q):
            if q == 0:
                if f > 0:
                    self.pens.append(pg.mkPen('r'))
                else:
                    self.pens.append(pg.mkPen('b'))
                self.brushes.append(pg.mkBrush('w'))
            else:
                if f > 0:
                    self.pens.append(pg.mkPen('r'))
                    self.brushes.append(pg.mkBrush('r'))
                else:
                    self.pens.append(pg.mkPen('b'))
                    self.brushes.append(pg.mkBrush('b'))
        # 10 is the nominal size
        npts = self.frequencies.shape[0]
        self.sizes = np.array(npts * [10])

        # use only poles and zeros with frequency different from zero
        idx = (self.frequencies != 0)
        self.frequencies = self.frequencies[idx]
        self.Q = self.Q[idx]
        self.idx = self.idx[idx]
        self.symbols = self.symbols[idx]
        self.vals = self.vals[idx]
        self.pos = self.pos[idx,:]
        self.sizes = self.sizes[idx]
        self.pens = [p for p,i in zip(self.pens, idx) if i]
        self.brushes = [b for b,i in zip(self.brushes, idx) if i]

        # 15 to show the selected one as a bigger marker
        if self.selected is not None:
            self.sizes[self.selected] = 15
        # build data dictionary and update graph
        self.data = {'pos': self.pos, 
                     'symbol': self.symbols, 
                     'size': self.sizes,
                     'data': np.empty(npts, dtype=[('index', int)])[idx],
                     'symbolPen': self.pens, 'symbolBrush': self.brushes
                    }
        self.data['data']['index'] = np.arange(self.pos.shape[0])
        self.updateGraph()
        
    def updateGraph(self):
        """
        Set data and update all marker positions
        """
        pg.GraphItem.setData(self, **self.data)
             
    def mouseDragEvent(self, ev):
        """
        Respond to the user dragging a marker
        """
        if ev.button() != QtCore.Qt.MouseButton.LeftButton:
            # not the left mouse button, ignore
            ev.ignore()
            return
        if self.parent.bFitMovePZ.isChecked():
            # do something only if the Move Poles/Zeros is selected
            if ev.isStart():
                # We are already one step into the drag.
                # Find the point(s) at the mouse cursor when the button was first 
                # pressed:

                pos = ev.buttonDownPos()
                pts = self.scatter.pointsAt(pos)
                if len(pts) == 0:
                    ev.ignore()
                    return
                self.dragPoint = pts[0]      # point being dragged
                self.ind = pts[0].data()[0]  # corresponding index

                # create copy of the transfer function without the pole or zero
                self.tf1 = self.tf.copy()
                if self.symbols[self.ind] == 'star':
                    self.tf1.remove_pole_fQ(self.frequencies[self.ind], self.Q[self.ind])
                else:
                    self.tf1.remove_zero_fQ(self.frequencies[self.ind], self.Q[self.ind])
                # compute frequency response of this base TF
                self.base_tf = self.tf1.fresp(self.parent.fit_fr)   # for fit plot
                self.base_tf_res = self.tf1.fresp(self.parent.fr)   # for residual plot
                # frequency bin and Laplace variables
                self.fr = self.parent.fit_fr
                self.s = 2j*np.pi*self.fr
                self.s_res = 2j*np.pi*self.parent.fr
                # frequency bin scale and offset, used to quickly convert frequency to array index
                self.fr_scale = (np.log10(self.fr[-1]) - np.log10(self.fr[0])) / self.fr.shape[0]
                self.fr_offset = np.log10(self.fr[0])
                # start with new gain and frequency of the pole or zero being moved
                self.new_k = 1
                self.new_freq = self.frequencies[self.ind]
                # save the sign of the frequency (needed for zeros with frequency <0)
                self.sign = np.sign(self.frequencies[self.ind])
                # start with same Q
                self.new_q = self.Q[self.ind]
                # offset between the initial drag point and the marker position
                self.dragOffset = self.data['pos'][self.ind] - pos

                # if the gain is locked at some frequency use that, otherwise lock at 0 Hz
                if self.parent.lock_gain_freq is not None:
                    self.lock_gain_freq = self.parent.lock_gain_freq
                else:
                    # decide at what frequency we can keep the gain constant. We pick 1/100th of the lowest pole or zero
                    frs = np.r_[self.tf1.z_f, self.tf1.p_f]
                    self.lock_gain_freq = frs[frs != 0].min()/100
                # compute gain lock value without the moving pole / zero
                self.lock_gain_value = np.abs(self.tf.fresp(self.lock_gain_freq) / self.tf1.fresp(self.lock_gain_freq))

            elif ev.isFinish():
                # the user released the mouse button, so drag is over
                self.dragPoint = None
                # we're done here, so we need to transfer the update to the fit object
                if self.symbols[self.ind] == 'star':
                    # add pole to transfer function and set gain
                    self.tf1.add_pole_fQ(self.new_freq, self.new_q)
                    self.tf1.set_gain(self.parent.fit_fr[0], np.abs(self.base_tf * self.pole)[0])
                else:
                    # add zero to transfer function and set gain
                    self.tf1.add_zero_fQ(self.new_freq, self.new_q)
                    self.tf1.set_gain(self.parent.fit_fr[0], np.abs(self.base_tf * self.zero)[0])
                # add previous TF to undo stack
                self.parent.undo.put(self.parent.tf_fit.copy())
                # move new transfer function to main transfer function and parent
                self.tf = self.tf1.copy()
                self.setData()
                self.parent.tf_fit = self.tf
                # replot
                self.parent.plotMeasurements()
                return
            else:
                # if we're draggin out of a point just forget about ii
                if self.dragPoint is None:
                    ev.ignore()
                    return

            # here we are in the middle of the drag, update pole/zero values and fit curves
            # get new frequency and peak value
            pos = ev.pos() + self.dragOffset
            new_freq = self.sign * 10**pos.x()
            new_peak = 10**pos.y()
            # find value of base transfer function and relative peak value at the new frequency
            idx = int(np.round((np.log10(abs(new_freq)) - self.fr_offset)/self.fr_scale)) # quick frequency to index
            base = self.base_tf[idx]
            # peak of the new pole or zero is the ratio of drag position over base TF
            new_peak = abs(new_peak/base)
            # compute new frequency and Q
            if self.Q[self.ind] == 0:
                # real pole or zero, change only frequency
                self.new_q = 0
                self.new_freq = new_freq
                if self.symbols[self.ind] == 'star':
                    # real pole, compute gain to maintain gain at lock frequency
                    self.new_k = self.lock_gain_value * np.abs((2j*np.pi*self.lock_gain_freq +
                                                                2 * np.pi * self.new_freq))
                    # transfer function of the pole for fit plots and residual plots
                    self.pole = self.new_k / (self.s + 2 * np.pi * self.new_freq)
                    pole_res = self.new_k / (self.s_res + 2 * np.pi * self.new_freq)
                    # update plot curves
                    self.parent.line_fit_abs.setData(self.fr, np.abs(self.base_tf * self.pole))
                    self.parent.line_fit_phi.setData(self.fr, 180/np.pi*np.angle(self.base_tf * self.pole))
                    self.parent.plot_res.disableAutoRange()
                    self.parent.line_meas_res.setData(self.parent.fr,
                                            np.abs((self.base_tf_res * pole_res - self.parent.tf) / self.parent.tf))
                    # set the marker position to follow the real pole position
                    pos.setX(np.log10(abs(self.new_freq) ))
                    pos.setY(np.log10(base * np.abs(self.new_k /
                                                    (2j* np.pi * self.new_freq + 2 * np.pi * self.new_freq)) ))

                    # update all markers
                    vals = np.abs(self.tf1.fresp(self.frequencies) * self.new_k /
                                                    (2j* np.pi * self.frequencies + 2 * np.pi * self.new_freq))
                    self.data['pos'] = np.c_[np.log10(np.abs(self.frequencies)), np.log10(vals)]
                    self.data['pos'][self.ind] = pos + self.dragOffset
                else:
                    # real zero, compute gain to maintain gain at lock frequency
                    self.new_k = np.sign(self.new_freq) * (self.lock_gain_value /
                                  np.abs((2j*np.pi*self.lock_gain_freq + 2 * np.pi * self.new_freq)))
                    # transfer function of the zero for fit plots and residual plots
                    self.zero = self.new_k * (self.s + 2 * np.pi * self.new_freq)
                    zero_res = self.new_k * (self.s_res + 2 * np.pi * self.new_freq)
                    # update plot curves
                    self.parent.line_fit_abs.setData(self.fr, np.abs(self.base_tf * self.zero))
                    self.parent.line_fit_phi.setData(self.fr, 180/np.pi*np.angle(self.base_tf * self.zero))
                    self.parent.plot_res.disableAutoRange()
                    self.parent.line_meas_res.setData(self.parent.fr,
                                                np.abs((self.base_tf_res * zero_res - self.parent.tf) / self.parent.tf))
                    # set the marker position to follow the real zero position
                    pos.setX(np.log10(abs(self.new_freq)))
                    pos.setY(np.log10(base * np.abs(self.new_k *
                                                (2j* np.pi * self.new_freq + 2 * np.pi * self.new_freq))))

                    # update all markers
                    vals = np.abs(self.tf1.fresp(self.frequencies) * self.new_k *
                                                (2j* np.pi * self.frequencies + 2 * np.pi * self.new_freq))
                    self.data['pos'] = np.c_[np.log10(np.abs(self.frequencies)), np.log10(vals)]
                    self.data['pos'][self.ind] = pos + self.dragOffset
            else:
                # complex poles or zeros
                if self.symbols[self.ind] == 'star':
                    # it's a complex pole, maintain gain at given frequency
                    w = 2*np.pi*self.lock_gain_freq
                    B = self.lock_gain_value
                    # these are the new peak value and frequency
                    w0 = 2*np.pi*new_freq  # peak frequency
                    A = new_peak           # peak value
                    # compute new gain and new Q
                    k = np.sqrt(B ** 2 * (w0 ** 2 - w ** 2) ** 2 / (1 - B ** 2 / A ** 2 * w ** 2 / w0 ** 2))
                    Q = A * w0 ** 2 / k

                    if not np.isnan(k) and not np.isnan(Q) and not np.isinf(k) and not np.isinf(Q):
                        # update only if the results is not a NaN (NaN happen when the Q would need to be negative)
                        self.new_k = k
                        self.new_q = Q
                        self.new_freq = new_freq
                        # compute the pole transfer function
                        self.pole = self.new_k / (self.s**2 + 2*np.pi*self.new_freq*self.s/self.new_q +
                                                  (2*np.pi*self.new_freq)**2)
                        pole_res = self.new_k / (self.s_res**2 + 2*np.pi*self.new_freq*self.s_res/self.new_q +
                                                 (2*np.pi*self.new_freq)**2)
                        # update plots
                        self.parent.line_fit_abs.setData(self.fr, np.abs(self.base_tf * self.pole))
                        self.parent.line_fit_phi.setData(self.fr, 180/np.pi*np.angle(self.base_tf * self.pole))
                        self.parent.plot_res.disableAutoRange()
                        self.parent.line_meas_res.setData(self.parent.fr, np.abs((self.base_tf_res * pole_res -
                                                                                  self.parent.tf) / self.parent.tf))
                        # update all markers
                        vals = np.abs(self.tf1.fresp(self.frequencies) *
                                      self.new_k / ((2j*np.pi*self.frequencies)**2
                                                    + 2*np.pi*self.new_freq*(2j*np.pi*self.frequencies)/self.new_q +
                                                    (2*np.pi*self.new_freq)**2))
                        self.data['pos'] = np.c_[np.log10(np.abs(self.frequencies)), np.log10(vals)]
                        self.data['pos'][self.ind] = ev.pos() + self.dragOffset
                else:
                    # it's a complex zero,  maintain gain at given frequency
                    w = 2*np.pi*self.lock_gain_freq
                    B = self.lock_gain_value
                    # these are the new peak value and frequency
                    w0 = 2 * np.pi * new_freq  # peak frequency
                    A = new_peak  # peak value
                    # compute new gain and new Q
                    k = 1/np.sqrt(1/B ** 2 * (w0 ** 2 - w ** 2) ** 2 / (1 - A ** 2 / B ** 2 * w ** 2 / w0 ** 2))
                    Q = k * w0 ** 2 / A
                    if not np.isnan(k) and not np.isnan(Q) and not np.isinf(k) and not np.isinf(Q):
                        # update only if the results is not a NaN (NaN happen when the Q would need to be negative)
                        self.new_k = k
                        self.new_q = Q
                        self.new_freq = new_freq
                        # compute the zero transfer function
                        self.zero = self.new_k * (self.s ** 2 + 2 * np.pi * self.new_freq * self.s / self.new_q + (
                                    2 * np.pi * self.new_freq) ** 2)
                        zero_res = self.new_k * (
                                    self.s_res ** 2 + 2 * np.pi * self.new_freq * self.s_res / self.new_q + (
                                        2 * np.pi * self.new_freq) ** 2)
                        # update plots
                        self.parent.line_fit_abs.setData(self.fr, np.abs(self.base_tf * self.zero))
                        self.parent.line_fit_phi.setData(self.fr, 180/np.pi*np.angle(self.base_tf * self.zero))
                        self.parent.plot_res.disableAutoRange()
                        self.parent.line_meas_res.setData(self.parent.fr, np.abs(
                            (self.base_tf_res * zero_res - self.parent.tf) / self.parent.tf))

                        # update all markers
                        vals = np.abs(self.tf1.fresp(self.frequencies) * self.new_k *
                                      ((2j*np.pi*self.frequencies)** 2 +
                                       2 * np.pi * self.new_freq * (2j*np.pi*self.frequencies) / self.new_q + (
                                    2 * np.pi * self.new_freq) ** 2))
                        self.data['pos'] = np.c_[np.log10(np.abs(self.frequencies)), np.log10(vals)]
                        self.data['pos'][self.ind] = ev.pos() + self.dragOffset
            # update all graphs and plots
            self.updateGraph()
            ev.accept()
        
    def clicked(self, pts, ev):
        """
        Method called when one marker is clicked
        """
        # index of clicked marker
        idx = ev[0].index()

        if self.parent.bFitDelete.isChecked():
            # we are deleting poles and zeros
            if self.symbols[idx] == 'o':
                # delete a zero, add current TTF to undo stack
                self.parent.undo.put(self.parent.tf_fit.copy())
                if self.parent.lock_gain_freq is None:
                    # remove the zero
                    self.tf.remove_zero_fQ(self.frequencies[idx], self.Q[idx])
                else:
                    # remove the zero, but maintain gain at lock frequency
                    gain = np.abs(self.tf.fresp(self.parent.lock_gain_freq))
                    self.tf.remove_zero_fQ(self.frequencies[idx], self.Q[idx])
                    self.tf.set_gain(self.parent.lock_gain_freq, gain)
            if self.symbols[idx] == 'star':
                # delete a pole, add current TTF to undo stack
                self.parent.undo.put(self.parent.tf_fit.copy())
                if self.parent.lock_gain_freq is None:
                    # remove the pole
                    self.tf.remove_pole_fQ(self.frequencies[idx], self.Q[idx])
                else:
                    # remove the pole, but maintain gain at lock frequency
                    gain = np.abs(self.tf.fresp(self.parent.lock_gain_freq))
                    self.tf.remove_pole_fQ(self.frequencies[idx], self.Q[idx])
                    self.tf.set_gain(self.parent.lock_gain_freq, gain)
            # update graphs and curves
            self.parent.tf_graph.updateTF(self.tf)
            self.parent.plotMeasurements()
        else:
            # we are clicking to select a marker
            if self.selected is None:
                # none was selected, remember new selection and increase size
                self.selected = idx
                self.data['size'][idx] = 15
            elif self.selected != idx:
                # clicking of a new marker, change selection
                self.data['size'][self.selected] = 10
                self.data['size'][idx] = 15
                self.selected = idx
            else:
                # clicking on the currently selected marker, unselect it
                self.data['size'][self.selected] = 10
                self.selected = None
            if self.selected is not None:
                # show the frequency and Q in the status bar
                if self.symbols[idx] == 'star':
                    self.parent.status_bar.showMessage(
                        'Selected pole fr=%f Hz, Q=%f' % (self.frequencies[idx], self.Q[idx]))
                else:
                    self.parent.status_bar.showMessage(
                        'Selected zero fr=%f Hz, Q=%f' % (self.frequencies[idx], self.Q[idx]))
            else:
                # clear status bar if nothing is selected
                self.parent.status_bar.clearMessage()
            self.updateGraph()
        # show the status bar widgets to adjust increments for keyboard change, if needed
        if self.selected is not None and self.parent.bFitMovePZ.isChecked():
            self.parent.status_bar_finput.show()
            self.parent.status_bar_flabel.show()
            self.parent.status_bar_qinput.show()
            self.parent.status_bar_qlabel.show()
        else:
            self.parent.status_bar_finput.hide()
            self.parent.status_bar_flabel.hide()
            self.parent.status_bar_qinput.hide()
            self.parent.status_bar_qlabel.hide()


########################################################################################################################
class CustomPlotItem(pg.PlotDataItem):
    """
    Custom class derived from PlotDataItem to handle drag events
    """
    def __init__(self, parent, *args, **kargs):
        super().__init__(*args, **kargs)
        # remember the parent class
        self.parent = parent
        # flag true when draggging the curve
        self.dragging = False
        # Need to switch off the "has no contents" flag (I don't understand this, copied from the Internet)
        self.setFlags(self.flags() & ~self.ItemHasNoContents)

    def mouseDragEvent(self, ev):
        if self.parent.bFitChangeGain.isChecked():
            # accept dragging only when the Change gain button is selected
            if ev.button() != QtCore.Qt.LeftButton:
                # and accept only left mouse buttons
                ev.ignore()
                return
            if ev.isStart():
                # check if the drag started close to the curve
                pos = ev.buttonDownPos()
                scale = self.parent.plot_abs.plotItem.vb.viewPixelSize()
                dist = np.min((self.curve.xData - pos.x())**2/scale[0]**2 + (self.curve.yData - pos.y())**2/scale[1]**2)
                if dist < 10:
                    # drag started close enough to the curve
                    ev.accept()
                    # add current transfer function to the undo stack
                    self.parent.undo.put(self.parent.tf_fit.copy())
                    # remember initial click position and gain
                    self.initial_pos = pos.y()
                    self.initial_k = self.parent.tf_fit.k
                    self.dragging = True
                else:
                    ev.ignore()
                    return
            elif ev.isFinish():
                # stoop draggging and replot
                self.dragging = False
                self.parent.tf_graph.setData()
                ev.accept()
            else:
                if self.dragging:
                    pos = ev.pos()
                    # update the transfer function gain with the new value
                    self.parent.tf_fit.set_k(self.initial_k * 10**(pos.y() - self.initial_pos))
                    if self.parent.lock_gain_value is not None:
                        self.parent.lock_gain_value = abs(self.parent.tf_fit.fresp(self.parent.lock_gain_freq))
                    self.parent.plot_res.disableAutoRange()
                    self.parent.tf_graph.updateTF(self.parent.tf_fit)
                    # replot everything
                    self.parent.plotMeasurements()
                    ev.accept()
                else:
                    ev.ignore()
        else:
            ev.ignore()

    # methods copied from the internet...
    def shape(self):
        # Inherit shape from the curve item
        return self.curve.shape()

    def boundingRect(self):
        # All graphics items require this method (unless they have no contents)
        return self.shape().boundingRect()

    def paint(self, p, *args):
        # All graphics items require this method (unless they have no contents)
        return

########################################################################################################################

class OpenFilesGUI(QtWidgets.QDialog):
    """
    Class to create the initial dialog to open data files
    """
    def __init__(self):
        super().__init__(parent=None)
        # empty variables
        self.fr = None
        self.transferfunction = None
        self.coherence = None
        self.session_filename = None

        # create window and resize
        self.setWindowTitle("Open data files...")
        self.resize(600,270)
        # grid layout for main widgets
        grid = QtWidgets.QGridLayout()
        self.setLayout(grid)
        # transfer function file input
        label_tf = QtWidgets.QLabel('Transfer function')
        label_tf.setAlignment(QtCore.Qt.AlignRight)
        grid.addWidget(label_tf, 0, 0)
        self.tf = QtWidgets.QLineEdit()
        grid.addWidget(self.tf, 0, 1)
        tf_button = QtWidgets.QPushButton("Select file...", self)
        grid.addWidget(tf_button, 0, 2)
        # transfer function format selection
        label_tf2 = QtWidgets.QLabel('Transfer function format')
        label_tf2.setAlignment(QtCore.Qt.AlignRight)
        grid.addWidget(label_tf2, 1, 0)
        self.tfb = [QtWidgets.QRadioButton("Frequency, Real, Imaginary"),
               QtWidgets.QRadioButton("Frequency, Abs, Phase [rad]"),
               QtWidgets.QRadioButton("Frequency, Abs, Phase [deg]"),
               QtWidgets.QRadioButton("Frequency, Abs [db], Phase [rad]"),
               QtWidgets.QRadioButton("Frequency, Abs [db], Phase [deg]")]
        self.tfb[0].setChecked(True)
        tf_group = QtWidgets.QButtonGroup()
        buttons = QtWidgets.QVBoxLayout()
        for b in self.tfb:
            tf_group.addButton(b)
            buttons.addWidget(b)
        grid.addLayout(buttons, 1,1)
        # coherence file input
        label_co = QtWidgets.QLabel('Coherence')
        label_co.setAlignment(QtCore.Qt.AlignRight)
        grid.addWidget(label_co, 2, 0)
        self.co = QtWidgets.QLineEdit()
        grid.addWidget(self.co, 2, 1)
        co_button = QtWidgets.QPushButton("Select file...", self)
        grid.addWidget(co_button, 2, 2)

        # add frequency range selection
        rangefr = QtWidgets.QHBoxLayout()
        grid.addLayout(rangefr, 4, 1)
        label_minfr = QtWidgets.QLabel('Min. freq.')
        label_minfr.setAlignment(QtCore.Qt.AlignRight)
        rangefr.addWidget(label_minfr)
        self.minfr = QtWidgets.QLineEdit()
        self.minfr.setValidator(pg.QtGui.QDoubleValidator())
        rangefr.addWidget(self.minfr)
        label_maxfr = QtWidgets.QLabel('Max. freq.')
        label_maxfr.setAlignment(QtCore.Qt.AlignRight)
        rangefr.addWidget(label_maxfr)
        self.maxfr = QtWidgets.QLineEdit()
        self.maxfr.setValidator(pg.QtGui.QDoubleValidator())
        rangefr.addWidget(self.maxfr)

        # add input text for pre-filter
        self.check_filt = QtWidgets.QCheckBox('Apply filter before fitting')
        grid.addWidget(self.check_filt, 6, 0)
        self.filter = QtWidgets.QLineEdit()
        self.filter.setText('TF(z=[], p=[], k=1)')
        grid.addWidget(self.filter, 6, 1)

        # add load and cancel buttons
        buttons2 = QtWidgets.QHBoxLayout()
        load_button = QtWidgets.QPushButton("Load", self)
        cancel_button = QtWidgets.QPushButton("Cancel", self)
        buttons2.addWidget(load_button)
        buttons2.addWidget(cancel_button)
        grid.addLayout(buttons2, 7, 0)
        # add stretch
        grid.setRowStretch(7, 1)
        grid.setColumnStretch(1, 1)

        # add load session button
        loadsession_button = QtWidgets.QPushButton("Load saved session", self)
        grid.addWidget(loadsession_button, 7, 1)

        # connect methods to signals
        tf_button.clicked.connect(self.open_tf)
        co_button.clicked.connect(self.open_co)
        load_button.clicked.connect(self.load_data)
        cancel_button.clicked.connect(self.cancel)
        loadsession_button.clicked.connect(self.loadsession)

    def cancel(self):
        """
        Called when the user clicks on Cancel
        """
        self.session_filename = None
        self.transferfunction = None
        self.close()

    def open_tf(self):
        """
        Open the system dialog to select a file
        """
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open transfer function data file", "",
                                                  "Text Files (*.txt);;All Files (*)")
        if fileName:
            self.tf.setText(fileName)
    def open_co(self):
        """
        Open the system dialog to select a file
        """
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open coherence data file", "",
                                                            "Text Files (*.txt);;All Files (*)")
        if fileName:
            self.co.setText(fileName)

    def load_data(self):
        # get file names from line edits
        tf_file = self.tf.text()
        co_file = self.co.text()
        # if no TF file specified, do nothing
        if tf_file == '':
            return
        # load datafile
        tf_data = np.loadtxt(tf_file, comments='#%')
        # extract data, depending on format
        self.fr = tf_data[:, 0]
        if self.tfb[0].isChecked():
            self.transferfunction = tf_data[:,1] + 1j*tf_data[:,2]
        if self.tfb[1].isChecked():
            self.transferfunction = tf_data[:, 1] * np.exp(1j*tf_data[:, 2])
        if self.tfb[2].isChecked():
            self.transferfunction = tf_data[:, 1] * np.exp(1j * tf_data[:, 2] * np.pi/180)
        if self.tfb[3].isChecked():
            self.transferfunction = 10**(tf_data[:, 1]/20) * np.exp(1j * tf_data[:, 2] )
        if self.tfb[4].isChecked():
            self.transferfunction = 10**(tf_data[:, 1]/20) * np.exp(1j * tf_data[:, 2] * np.pi / 180)

        # check if we need to apply a filter
        if self.check_filt.isChecked():
            try:
                filt = eval(self.filter.text())
            except Exception as err:
                dlg = QtWidgets.QMessageBox(self)
                dlg.setWindowTitle('Error in filter definition')
                dlg.setText("Error in filter definition: " + str(type(err).__name__) + ' - ' + str(err))
                dlg.exec()
                return
            
            self.transferfunction *= filt.fresp(self.fr)

        # load coherence
        if co_file != '':
            co_data = np.loadtxt(co_file, comments='#%')
            fr = co_data[:,0]
            co = co_data[:,1]
            # make sure the coherence is sampled at the same bins as the transfer function
            self.coherence = np.interp(self.fr, fr, co)
        else:
            self.coherence = None

        # check if the user specified frequency limits
        if self.minfr.text():
            fmin = float(self.minfr.text())
        else:
            fmin = -1
        if self.maxfr.text():
            fmax = float(self.maxfr.text())
        else:
            fmax = np.Inf
        idx = (self.fr>fmin)*(self.fr<fmax)
        self.fr = self.fr[idx]
        self.transferfunction = self.transferfunction[idx]
        if self.coherence is not None:
            self.coherence = self.coherence[idx]

        # all done, close this window
        self.close()

    def loadsession(self):
        # open a dialog to select a saved session file
        fileName, ok = QtWidgets.QFileDialog.getOpenFileName(self, "Open saved session", "",
                                                                   "All Files (*)")
        if ok:
            self.session_filename = fileName
            # all done, close this window, the parent class will take care of loading the session
            self.close()


########################################################################################################################

class OptimizationGUI(QtWidgets.QDialog):
    """
    Class to create a dialog window with options for the optimization of pole and zero values
    """
    # signal emitted when done with the optimization
    opt_done = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__(parent=parent.win)
        # pointer to the main InteractiveFitting class
        self.parent = parent
        # allowed optimization methods
        self.methods = ['Default', 'L-BFGS-B', 'Nelder-Mead', 'Powell', 'TNC']
        # create window
        self.setWindowTitle("Optimize fit")
        #self.resize(600,350)
        # grid layout for main widgets
        grid = QtWidgets.QGridLayout()
        self.grid = grid
        self.setLayout(grid)
        # select method
        label1 = QtWidgets.QLabel('Optimization method')
        grid.addWidget(label1, 0, 0)
        self.combo = QtWidgets.QComboBox()
        for i in self.methods:
            self.combo.addItem(i)
        self.combo.setCurrentIndex(0)
        grid.addWidget(self.combo, 0, 1, 1, 2)
        # bounds
        label2 = QtWidgets.QLabel('Relative change in frequency (min,max)')
        grid.addWidget(label2, 1, 0)
        self.minfr = QtWidgets.QLineEdit()
        self.minfr.setValidator(pg.QtGui.QDoubleValidator(0.0, 1.0, 3))
        grid.addWidget(self.minfr, 1, 1)
        self.maxfr = QtWidgets.QLineEdit()
        self.maxfr.setValidator(pg.QtGui.QDoubleValidator(1.0, np.Inf, 3))
        grid.addWidget(self.maxfr, 1, 2)
        label3 = QtWidgets.QLabel('Relative change in Q (min, max)')
        grid.addWidget(label3, 2, 0)
        self.minq = QtWidgets.QLineEdit()
        self.minq.setValidator(pg.QtGui.QDoubleValidator(0.0, 1.0, 3))
        grid.addWidget(self.minq, 2, 1)
        self.maxq = QtWidgets.QLineEdit()
        self.maxq.setValidator(pg.QtGui.QDoubleValidator(1.0, np.Inf, 3))
        grid.addWidget(self.maxq, 2, 2)

        label3b = QtWidgets.QLabel('Relative change in gain (min, max)')
        grid.addWidget(label3b, 3, 0)
        self.ming = QtWidgets.QLineEdit()
        self.ming.setValidator(pg.QtGui.QDoubleValidator(0.0, 1.0, 3))
        grid.addWidget(self.ming, 3, 1)
        self.maxg = QtWidgets.QLineEdit()
        self.maxg.setValidator(pg.QtGui.QDoubleValidator(1.0, np.Inf, 3))
        grid.addWidget(self.maxg, 3, 2)
        self.minfr.setText('%.3f' % 0.5)
        self.maxfr.setText('%.3f' % 2)
        self.minq.setText('%.3f' % 0.5)
        self.maxq.setText('%.3f' % 2)
        self.ming.setText('%.3f' % 0.5)
        self.maxg.setText('%.3f' % 2)
        label4 = QtWidgets.QLabel('Absolute frequency (min,max)')
        grid.addWidget(label4, 4, 0)
        self.absminfr = QtWidgets.QLineEdit()
        self.absminfr.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.absminfr, 4, 1)
        self.absmaxfr = QtWidgets.QLineEdit()
        self.absmaxfr.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.absmaxfr, 4, 2)
        label5 = QtWidgets.QLabel('Max Q value')
        grid.addWidget(label5, 5, 0)
        self.absmaxq = QtWidgets.QLineEdit()
        self.absmaxq.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.absmaxq, 5, 1)
        self.norm_lin = QtWidgets.QRadioButton("Linear-in-frequency normalization")
        self.norm_log = QtWidgets.QRadioButton("Logarithmic-in-frequency normalization")
        self.norm_log.setChecked(True)
        tf_group = QtWidgets.QButtonGroup()
        buttons = QtWidgets.QVBoxLayout()
        tf_group.addButton(self.norm_lin)
        buttons.addWidget(self.norm_lin)
        tf_group.addButton(self.norm_log)
        buttons.addWidget(self.norm_log)
        grid.addLayout(buttons, 6, 1, 1, 2)

        # status line
        self.status = QtWidgets.QLabel("Select method, set relative or absolute bounds for frequency and Q, "
                                       "then run optimizer")
        self.status.setStyleSheet("background-color: white")
        grid.addWidget(self.status, 7, 0, 1, 3)

        # # allow adding poles and zeros
        # self.add_zp_label = QtWidgets.QLabel('Allow adding zero/poles during optimization (how many)')
        # grid.addWidget(self.adv_label1, 8, 0)
        # self.add_zp = QtWidgets.QLineEdit()
        # self.add_zp.setValidator(pg.QtGui.QIntValidator(0, 100))
        # grid.addWidget(self.add_zp, 8, 1)

        # advanced constraints
        self.advanced = QtWidgets.QCheckBox('Limit gain in band (1)', self)
        self.advanced.toggled.connect(self.toggle_advanced)
        grid.addWidget(self.advanced, 9, 0, 1, 3)
        self.adv_label1 = QtWidgets.QLabel('Limit gain to a maximum of')
        grid.addWidget(self.adv_label1, 10, 0)
        self.adv_maxg = QtWidgets.QLineEdit()
        self.adv_maxg.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.adv_maxg, 10, 1)
        self.adv_label2 = QtWidgets.QLabel('In frequency range (min, max) [Hz]')
        grid.addWidget(self.adv_label2, 11, 0)
        self.adv_minfr = QtWidgets.QLineEdit()
        self.adv_minfr.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.adv_minfr, 11, 1)
        self.adv_maxfr = QtWidgets.QLineEdit()
        self.adv_maxfr.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.adv_maxfr, 11, 2)

        self.advanced2 = QtWidgets.QCheckBox('Limit gain in band (2)', self)
        self.advanced2.toggled.connect(self.toggle_advanced)
        grid.addWidget(self.advanced2, 12, 0, 1, 3)
        self.adv_label3 = QtWidgets.QLabel('Limit gain to a maximum of')
        grid.addWidget(self.adv_label3, 13, 0)
        self.adv_maxg2 = QtWidgets.QLineEdit()
        self.adv_maxg2.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.adv_maxg2, 13, 1)
        self.adv_label4 = QtWidgets.QLabel('In frequency range (min, max) [Hz]')
        grid.addWidget(self.adv_label4, 14, 0)
        self.adv_minfr2 = QtWidgets.QLineEdit()
        self.adv_minfr2.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.adv_minfr2, 14, 1)
        self.adv_maxfr2 = QtWidgets.QLineEdit()
        self.adv_maxfr2.setValidator(pg.QtGui.QDoubleValidator(0.0, np.Inf, 3))
        grid.addWidget(self.adv_maxfr2, 14, 2)

        # initially hide them
        self.adv_label1.hide()
        self.adv_maxg.hide()
        self.adv_label2.hide()
        self.adv_minfr.hide()
        self.adv_maxfr.hide()
        self.adv_label3.hide()
        self.adv_maxg2.hide()
        self.adv_label4.hide()
        self.adv_minfr2.hide()
        self.adv_maxfr2.hide()

        # buttons
        buttons2 = QtWidgets.QHBoxLayout()
        run_button = QtWidgets.QPushButton("Run", self)
        cancel_button = QtWidgets.QPushButton("Cancel", self)
        buttons2.addWidget(run_button)
        buttons2.addWidget(cancel_button)
        grid.addLayout(buttons2, 15, 0)

        # connect methods
        cancel_button.clicked.connect(self.close)
        run_button.clicked.connect(self.optimize)

    def close(self):
        """
        Called when the cancel button is clicked
        """
        self.opt_done.emit()
        super().close()

    def toggle_advanced(self):
        """
        Toggle visibility of advanced constraint parameters
        """
        if self.advanced.isChecked():
            self.adv_label1.show()
            self.adv_maxg.show()
            self.adv_label2.show()
            self.adv_minfr.show()
            self.adv_maxfr.show()
            self.setFixedSize(self.grid.sizeHint())
        else:
            self.adv_label1.hide()
            self.adv_maxg.hide()
            self.adv_label2.hide()
            self.adv_minfr.hide()
            self.adv_maxfr.hide()
            self.setFixedSize(self.grid.sizeHint())
        if self.advanced2.isChecked():
            self.adv_label3.show()
            self.adv_maxg2.show()
            self.adv_label4.show()
            self.adv_minfr2.show()
            self.adv_maxfr2.show()
            self.setFixedSize(self.grid.sizeHint())
        else:
            self.adv_label3.hide()
            self.adv_maxg2.hide()
            self.adv_label4.hide()
            self.adv_minfr2.hide()
            self.adv_maxfr2.hide()
            self.setFixedSize(self.grid.sizeHint())
    def optimize(self):
        """
        Run optimization algorithm
        """

        # build initial parameter vector (frequency, Q and gain are relative to current for better scaling)
        p0 = np.r_[np.ones_like(self.parent.tf_fit.z_f),
                   np.ones_like(self.parent.tf_fit.p_f),
                   np.ones_like(self.parent.tf_fit.z_Q),
                   np.ones_like(self.parent.tf_fit.p_Q), 1]

        # bounds (relative to current values)
        fmin = float(self.minfr.text())
        fmax = float(self.maxfr.text())
        qmin = float(self.minq.text())
        qmax = float(self.maxq.text())
        gmin = float(self.ming.text())
        gmax = float(self.maxg.text())
        bounds = (self.parent.tf_fit.z_f.shape[0] * [[fmin, fmax]] +
                  self.parent.tf_fit.p_f.shape[0] * [[fmin, fmax]] +
                  self.parent.tf_fit.z_Q.shape[0] * [[qmin, qmax]] +
                  self.parent.tf_fit.p_Q.shape[0] * [[qmin, qmax]] +
                  [[gmin, gmax]])
        # absolute bounds
        if self.absminfr.text() != '':
            afmin = float(self.absminfr.text())
        else:
            afmin = 0
        if self.absmaxfr.text() != '':
            afmax = float(self.absmaxfr.text())
        else:
            afmax = np.Inf
        if self.absmaxq.text() != '':
            aqmax = float(self.absmaxq.text())
        else:
            aqmax = np.Inf
        bmin = np.r_[afmin/abs(self.parent.tf_fit.z_f), afmin/self.parent.tf_fit.p_f,
                     np.zeros_like(self.parent.tf_fit.z_Q), np.zeros_like(self.parent.tf_fit.p_Q), 0]
        bmax = np.r_[afmax / abs(self.parent.tf_fit.z_f), afmax / self.parent.tf_fit.p_f,
                     aqmax / self.parent.tf_fit.z_Q, aqmax / self.parent.tf_fit.p_Q, np.Inf]

        # take the most restrictive min and max
        bounds = [[max(bounds[i][0], bmin[i]), min(bounds[i][1], bmax[i])] for i in range(len(bounds))]

        # extract fit frequencies from the ranges
        self.parent.fit_idx = np.zeros_like(self.parent.fr, dtype=bool)
        for i, lr in enumerate(self.parent.fitAreas):
            fmin, fmax = lr.getRegion()
            fmin = 10 ** fmin
            fmax = 10 ** fmax
            self.parent.fit_idx[(self.parent.fr > fmin) * (self.parent.fr < fmax)] = True

        # define cost function
        def cost(p, scale):
            # convert parameter vector to poles, zeros and gains
            n_z = self.parent.tf_fit.z_f.shape[0]
            n_p = self.parent.tf_fit.p_f.shape[0]
            z_f = self.parent.tf_fit.z_f*p[0:n_z]
            p_f = self.parent.tf_fit.p_f*p[n_z:n_z+n_p]
            z_Q = self.parent.tf_fit.z_Q*p[n_z+n_p:2*n_z+n_p]
            p_Q = self.parent.tf_fit.p_Q*p[2*n_z+n_p:2*n_z+2*n_p]
            k = self.parent.tf_fit.k * p[-1]
            # build transfer function
            fit = TF()
            fit.set_fQ(z_f, z_Q, p_f, p_Q, k)
            # compute residual
            res = (abs(self.parent.tf[self.parent.fit_idx] - fit.fresp(self.parent.fr[self.parent.fit_idx])) /
                   abs(self.parent.tf[self.parent.fit_idx]))**2
            # weight the residual so that every frequency has the same importance in log scale
            if self.norm_log.isChecked():
                w = 1/self.parent.fr[self.parent.fit_idx]
            else:
                w = np.ones_like(self.parent.fr[self.parent.fit_idx])
            w = w / sum(w)
            return scale*(w*res).mean()

        # define constrain function, if needed
        self.constraints = []
        if self.adv_maxg.text() != '' and self.adv_minfr.text() != '' and self.adv_maxfr.text() != '':
            # frequency bins
            fr = np.linspace(float(self.adv_minfr.text()), float(self.adv_maxfr.text()), self.parent.fit_nfr)
            # compute the maximum gain in the band
            def max_gain(p):
                # convert parameter vector to poles, zeros and gains
                n_z = self.parent.tf_fit.z_f.shape[0]
                n_p = self.parent.tf_fit.p_f.shape[0]
                z_f = self.parent.tf_fit.z_f * p[0:n_z]
                p_f = self.parent.tf_fit.p_f * p[n_z:n_z + n_p]
                z_Q = self.parent.tf_fit.z_Q * p[n_z + n_p:2 * n_z + n_p]
                p_Q = self.parent.tf_fit.p_Q * p[2 * n_z + n_p:2 * n_z + 2 * n_p]
                k = self.parent.tf_fit.k * p[-1]
                # build transfer function
                fit = TF()
                fit.set_fQ(z_f, z_Q, p_f, p_Q, k)
                return abs(fit.fresp(fr)).max()/float(self.adv_maxg.text())
            # non-linear constraint, maximum gain between 0 and 1
            self.constraints.append(scipy.optimize.NonlinearConstraint(max_gain, 0, 1))

        if self.adv_maxg2.text() != '' and self.adv_minfr2.text() != '' and self.adv_maxfr2.text() != '':
            # frequency bins
            fr2 = np.linspace(float(self.adv_minfr2.text()), float(self.adv_maxfr2.text()), self.parent.fit_nfr)
            # compute the maximum gain in the band
            def max_gain2(p):
                # convert parameter vector to poles, zeros and gains
                n_z = self.parent.tf_fit.z_f.shape[0]
                n_p = self.parent.tf_fit.p_f.shape[0]
                z_f = self.parent.tf_fit.z_f * p[0:n_z]
                p_f = self.parent.tf_fit.p_f * p[n_z:n_z + n_p]
                z_Q = self.parent.tf_fit.z_Q * p[n_z + n_p:2 * n_z + n_p]
                p_Q = self.parent.tf_fit.p_Q * p[2 * n_z + n_p:2 * n_z + 2 * n_p]
                k = self.parent.tf_fit.k * p[-1]
                # build transfer function
                fit = TF()
                fit.set_fQ(z_f, z_Q, p_f, p_Q, k)
                return abs(fit.fresp(fr2)).max()/float(self.adv_maxg2.text())
            # non-linear constraint, maximum gain between 0 and 1
            self.constraints.append(scipy.optimize.NonlinearConstraint(max_gain2, 0, 1))
 
        # callback function to update message
        def callback(res):
            self.status.setText('Current cost function value: %.5f' % cost(res, self.scale))

        # function that gets called when the optimization is over
        def optimization_finished():
            # save previous TF in undo stack
            self.parent.undo.put(self.parent.tf_fit.copy())
            # update current TF
            n_z = self.parent.tf_fit.z_f.shape[0]
            n_p = self.parent.tf_fit.p_f.shape[0]
            z_f = self.parent.tf_fit.z_f * self.res.x[0:n_z]
            p_f = self.parent.tf_fit.p_f * self.res.x[n_z:n_z + n_p]
            z_Q = self.parent.tf_fit.z_Q * self.res.x[n_z + n_p:2 * n_z + n_p]
            p_Q = self.parent.tf_fit.p_Q * self.res.x[2 * n_z + n_p:2 * n_z + 2 * n_p]
            k = self.parent.tf_fit.k * self.res.x[-1]
            self.parent.tf_fit = TF()
            self.parent.tf_fit.set_fQ(z_f, z_Q, p_f, p_Q, k)
            # update plots
            self.parent.tf_graph.updateTF(self.parent.tf_fit)
            self.parent.plotMeasurements()
            # close window
            self.opt_done.emit()
            self.status.setText("Select method, set relative or absolute bounds for frequency and Q, then run optimizer")
            self.close()

        # prepare the infrastructure to run the optimization in a separate thread
        class WorkerSignals(QtCore.QObject):
            finished = QtCore.pyqtSignal()
        class Worker(QtCore.QRunnable):
            def __init__(self, p0, bounds, cost, method, constraints, parent):
                super(Worker, self).__init__()
                self.p0 = p0
                self.bounds = bounds
                self.cost = cost
                self.method = method
                self.signals = WorkerSignals()
                self.parent = parent
                self.constraints = constraints
            def run(self):
                # minimization
                from scipy.optimize import minimize
                self.parent.scale = 1 / cost(self.p0, 1)
                if self.method == 'Default':
                    self.method = None
                self.parent.res = minimize(self.cost, self.p0, bounds=self.bounds, constraints=self.constraints,
                                   args=(self.parent.scale,), callback=callback, method=self.method)
                print(self.parent.res)
                self.signals.finished.emit()

        # do the actual minimization
        worker = Worker(p0, bounds, cost, self.combo.currentText(), self.constraints, self)
        worker.signals.finished.connect(optimization_finished)
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.start(worker)

class FitDialog(QtWidgets.QDialog):
    """
    Class to create a dialog window with options for running vectfit3
    """
    def __init__(self, parent=None):
        super().__init__()
        self.ok = False  # will be set to True when ok is clicked
        self.setWindowTitle("Run VectFit3")
        self.grid = QtWidgets.QGridLayout()
        self.setLayout(self.grid)
        label1 = QtWidgets.QLabel("Select the order of the fit. You can write a single number, a list, or a valid python expression\n" +
            "that return a list of values, for example 'np.arange(2,21,2)'  to use all even orders between\n" +
            "2 and 20, with steps of 2; or or '8,10,12' to specify a list of orders.")
        self.grid.addWidget(label1, 0, 0, 1, 3)
        self.order = QtWidgets.QLineEdit()
        self.grid.addWidget(self.order, 1, 0, 1, 3)
        self.delay_check = QtWidgets.QCheckBox('Try to fit delay (min,max [s])')
        self.delay_check.toggled.connect(self.toggle_check)
        self.grid.addWidget(self.delay_check, 2, 0, 1, 1)
        self.dmin = QtWidgets.QLineEdit()
        self.dmax = QtWidgets.QLineEdit()
        self.dmin.setValidator(pg.QtGui.QDoubleValidator())
        self.dmax.setValidator(pg.QtGui.QDoubleValidator())
        self.grid.addWidget(self.dmin, 2, 1, 1, 1)
        self.grid.addWidget(self.dmax, 2, 2, 1, 1)
        self.dmin.setEnabled(False)
        self.dmax.setEnabled(False)
        self.delay_pade = QtWidgets.QRadioButton('Use Pade approximation')
        self.delay_exp  = QtWidgets.QRadioButton('Keep delay as phase')
        group = QtWidgets.QButtonGroup()
        group.addButton(self.delay_pade)
        group.addButton(self.delay_exp)
        self.delay_pade.setEnabled(False)
        self.delay_exp.setEnabled(False)
        self.delay_exp.setChecked(True)
        self.grid.addWidget(self.delay_pade, 3, 1, 1, 1)
        self.grid.addWidget(self.delay_exp, 3, 2, 1, 1)
        self.button_ok = QtWidgets.QPushButton("Ok")
        self.button_cancel = QtWidgets.QPushButton("Cancel")
        self.grid.addWidget(self.button_ok, 4, 1, 1, 1)
        self.grid.addWidget(self.button_cancel, 4, 2, 1, 1)
        self.button_cancel.clicked.connect(self.close)
        self.button_ok.clicked.connect(self.clicked_ok)

    def clicked_ok(self):
        # called when user clicks ok
        self.ok = True
        self.close()
    def toggle_check(self):
        # update enable fields depedning on what's checked
        self.dmin.setEnabled(self.delay_check.isChecked())
        self.dmax.setEnabled(self.delay_check.isChecked())
        self.delay_pade.setEnabled(self.delay_check.isChecked())
        self.delay_exp.setEnabled(self.delay_check.isChecked())
        if self.dmin.text() == '':
            self.dmin.setText('0')
        if self.dmax.text() == '':
            self.dmax.setText('1e-3')
