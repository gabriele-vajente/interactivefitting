# InteractiveFitting

Graphical user interface to fit transfer functions with VectFit3. And to allow tuning the result by manually moving
or adding/removing poles and zeros.

Distributed under GNU General Public License 3.0, see [LICENSE](LICENSE) for more information.

Gabriele Vajente (vajente@caltech.edu) - 2024-01-17

### Class Constructor Parameters

- fr: numpy.ndarray (float)
    - frequency bins
- tf: numpy.ndarray (complex)
    - transfer function values (complex number) for each frequency bin
- co: numpy.ndarray (float)
    - optional, coherence value for each frequency bin
- fit: TF or [numpy.ndarray, numpy.ndarray, numpy.ndarray]
    - optional, pass the fit, either as a TransferFunction object, or as a list [z, p, k]

### Examples

`InteractiveFitting(fr, tf)` will open the GUI with the measurement defined by the frequency bins in `fr`
and the complex transfer function `tf`. Once the GUI is opne, you can define the fitting region, run VectFit
and tweak the results

`InteractiveFitting(fr, tf, coherence=co)` same as above, but now you're passing the coherence vector `co` and you
can adjust a coherence threshold for fitting in the GUI.

`InteractiveFitting(fr, tf, fit=[z, p, k])` instead of running VectFit, you're passing a set of poles `p`, zeros `z`
and gain `k` in s-domain, that defines the fit that you can tweak with the GUI.

`InteractiveFitting()` when called without arguments, opens a dialog to select files from where data can be read.

Tooltips appear when hovering over any button, to explain the function.

### Tutorial

Please refer to [interactivefitting/TUTORIAL.md](interactivefitting/TUTORIAL.md) for a step by step tutorial

![interactivefitting](interactivefitting.png)
