from setuptools import setup

with open('README.md', 'rb') as f:
    longdesc = f.read().decode().strip()

setup(
    name='interactivefitting',
    version='0.1.1',
    description="Graphical user interface to fit transfer functions",
    long_description=longdesc,
    long_description_content_type='text/markdown',
    author='Gabriele Vajente',
    author_email='gabriele.vajente@ligo.org',
    url='https://git.ligo.org/gabriele-vajente/interactivefitting',
    license='GPL-3.0-or-later',
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Development Status :: 5 - Production/Stable',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],

    install_requires=[
        'numpy',
        'scipy',
        'pyqtgraph',
        'QtPy',
        'foton'
    ],

    packages=[
        'interactivefitting',
    ],

    include_package_data=True,

    package_data={
        '': ['interactivefitting/sample_data/*',
             'interactivefitting/pics/*',
             'interactivefitting/TUTORIAL.md'],
    },
)
